import { combineReducers } from 'redux';

import app from '../modules/AppState'
import user from '../modules/profile/personalArea/PersonalAreaState'

const rootReducer = combineReducers({
    app,
    user
});

export default rootReducer
