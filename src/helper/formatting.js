let getPrice = function(price){
    if (typeof price != 'number'){
        return 0
    }
    //
    // price = new Intl.NumberFormat('ru-RU', {
    //     style: 'currency',
    //     currencyDisplay: 'symbol',
    //     currency: 'RUB'
    // }).format(price)

    price = price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$& ');

    return price + '₽'
}
let getTimeHours = function (time) {
    var hours = Math.floor(time / 60);
    var minutes = Math.floor((time - (hours * 60)));

    if (hours < 10) {
        hours = hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    return hours + ' час ' + minutes + ' минут';
}
let getCountOrder = function (order) {
    order = '000000' + order
    order = order.slice(-4)

    return order
}
let timestampToDate = function (timestamp) {
    if (!timestamp){
        return 'Не выбрано'
    }

    var date = new Date(timestamp);
    var year = date.getFullYear();
    var month = ('0' + (date.getMonth() + 1)).slice(-2);
    var day = ('0' + date.getDate()).slice(-2);
    var convdataTime = day+'.'+month+'.'+year;

    return convdataTime
}
let timestampToTime = function (timestamp) {
    if (!timestamp){
        return 'Не выбрано'
    }

    var a = new Date(timestamp);
    var hour = ('0' + a.getHours()).slice(-2);
    var min = ('0' + a.getMinutes()).slice(-2);
    var time = hour + ':' + min;

    return time;
}
let getDateFromBackend = function (date) {
    if ( date ){
        date = date.split('-').reverse().join('.')

        return date
    }

    return 'Не задано'
}
let getDateToBackend = function (date) {
    if ( date ){
        date = date.split('.').reverse().join('-')

        return date
    }

    return 'Не задано'
}

export { getPrice, getTimeHours, getCountOrder, timestampToDate, timestampToTime, getDateFromBackend, getDateToBackend }
