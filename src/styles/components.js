export default {
    input: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: 'white',
        borderRadius: 2,
        height: 45,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textarea: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: 'white',
        borderRadius: 2,
        justifyContent: 'center'
    },

    flatLoader: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
}
