export default {
    primary: '#da7b9e',
    primaryOverlay: 'rgba(218, 123, 158, 0.5);',
    error: '#539CC6',
    placeholder: '#D5D5D5',
    page: '#f5f5f5'
}
