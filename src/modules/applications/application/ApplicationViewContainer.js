// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ApplicationView from './ApplicationView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(ApplicationView);
