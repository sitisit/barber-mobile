import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Image
} from 'react-native';
import MapView, {Marker, Callout} from 'react-native-maps';
import {Icon, Button} from 'native-base';
import ModalHideBottom from "../../../components/ModalHideBottom";
import {getPrice, getTimeHours} from "../../../helper/formatting";
import {LoaderApp} from "../../../components/LoaderApp";
import {DropDownHolder} from "../../../components/DropDownAlert";
import color from "../../../styles/color";
import axios from "../../../plugins/axios";

// type 0 - Заявки в работе
// type 1 - Выполненые заявки
// type 2 - Отмененные заявки

class Application extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: null,
            type: null,

            application: {},

            openMap: false,
        }
    }

    componentDidMount = () => {
        LoaderApp.isChangeLoaderApp(true)

        this.setState({
            id: this.props.navigation.state.params.id
        })

        this.loadApplication()
    }

    takeWork = () => {
        LoaderApp.isChangeLoaderApp(true)

        let body = {
            request_id: this.state.id
        }
        axios('post', 'api/v1/takerequest', body).then(response => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Заявка принята в работе')
            this.props.navigation.navigate('ApplicationMyPage', {
                id: '',
                type: 0
            })
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    loadApplication = () => {
        axios('get', 'api/v1/request-view?id=' + this.props.navigation.state.params.id).then(response => {
            console.log(response.data)

            LoaderApp.isChangeLoaderApp(false)
            this.setState({
                application: response.data
            })
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            LoaderApp.isChangeLoaderApp(false)
        })
    }

    render() {
        const application = this.state.application
        if (Object.keys(application).length == 0) {
            return (
                <View style={styles.page}></View>
            )
        }

        return (
            <View style={styles.page}>
                <ScrollView
                    style={{flex: 1}}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{padding: 20}}
                >
                    <View style={styles.top}>
                        <Text style={styles.name}>{application.service_name}</Text>
                        <Text style={styles.category}>{application.category_name}</Text>
                    </View>
                    <View style={styles.lineInfo}>
                        <View style={styles.lineInfoItem}>
                            <View style={[styles.lineInfoItemIcon, {backgroundColor: '#308c65'}]}>
                                <Icon
                                    name="ruble-sign"
                                    type="FontAwesome5"
                                    style={{color: 'white', fontSize: 10}}
                                />
                            </View>
                            <Text style={styles.lineInfoItemText}>
                                {getPrice(application.master_price)} - вознаграждение мастера
                            </Text>
                        </View>
                        <View style={styles.lineInfoItem}>
                            <View style={[styles.lineInfoItemIcon, {backgroundColor: '#308c65'}]}>
                                <Icon
                                    name="ruble-sign"
                                    type="FontAwesome5"
                                    style={{color: 'white', fontSize: 10}}
                                />
                            </View>
                            <Text style={styles.lineInfoItemText}>
                                {getPrice(application.total_price)} - стоимость для клиента
                            </Text>
                        </View>
                        <View style={styles.lineInfoItem}>
                            <View style={[styles.lineInfoItemIcon, {backgroundColor: '#0a6c82'}]}>
                                <Icon
                                    name="clock"
                                    type="FontAwesome5"
                                    style={{color: 'white', fontSize: 10}}
                                />
                            </View>
                            <Text style={styles.lineInfoItemText}>~ {getTimeHours(application.lead_time)} - время
                                выполнения заявки</Text>
                        </View>
                    </View>
                    <View style={styles.sectionItem}>
                        <Text style={styles.text}>
                            <Text style={styles.label}>Дата и время ожидание: </Text>
                            {application.want_date} {application.want_time}
                        </Text>
                    </View>
                    {/*<View style={styles.sectionItem}>*/}
                    {/*    <Text style={styles.text}>*/}
                    {/*        <Text style={styles.label}>Длина волос: </Text>*/}
                    {/*        Короткие(статика)*/}
                    {/*    </Text>*/}
                    {/*</View>*/}
                    <View style={[styles.sectionItem, {flexDirection: 'column'}]}>
                        <Text style={[styles.text, {marginBottom: 10}]}>
                            <Text style={styles.label}>Адрес: </Text>
                            {application.location}
                        </Text>
                        {
                            (false) && (
                                <Button bordered full primary small onPress={() => this.setState({openMap: true})}>
                                    <Text style={{color: '#da7b9e'}}>Показать на карте</Text>
                                </Button>
                            )
                        }
                    </View>

                    {
                        (application.user_comment) ? (
                            <View style={styles.blockComment}>
                                <Text style={styles.blockCommentLabel}>Комментарий пользователя</Text>
                                <View style={styles.blockCommentContent}>
                                    <Text style={styles.blockCommentText}>{application.user_comment}</Text>
                                </View>
                            </View>
                        ) : (<View></View>)
                    }
                    {
                        (application.operator_comment) ? (
                            <View style={styles.blockComment}>
                                <Text style={styles.blockCommentLabel}>Комментарий оператора</Text>
                                <View style={styles.blockCommentContent}>
                                    <Text style={styles.blockCommentText}>{application.operator_comment}</Text>
                                </View>
                            </View>
                        ) : (<View></View>)
                    }

                </ScrollView>
                <View style={styles.bottomContent}>
                    <Button full primary onPress={() => this.takeWork()}>
                        <Text style={{color: 'white'}}>Выполнить</Text>
                    </Button>
                </View>

                <ModalHideBottom
                    open={this.state.openMap}
                    close={() => {
                        this.setState({openMap: false})
                    }}
                >
                    <MapView
                        initialRegion={{
                            latitude: 56.8519000,
                            longitude: 60.6122000,
                            latitudeDelta: 0.1,
                            longitudeDelta: 0.1,
                        }}
                        showsBuildings={true}
                        showsIndoorLevelPicker={true}
                        showsUserLocation={true}
                        loadingEnabled={true}
                        showsIndoorLevelPicker={true}
                        showsMyLocationButton={false}
                        style={{flex: 1}}
                        loadingIndicatorColor={'#da7b9e'}
                    />
                </ModalHideBottom>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Заявка',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: color.page
    },
    top: {
        marginBottom: 10
    },
    name: {
        fontSize: 18,
        lineHeight: 20,
        fontWeight: 'bold'
    },
    category: {
        fontSize: 20,
        lineHeight: 22
    },
    image: {
        flex: 1
    },

    lineInfo: {
        flexWrap: 'wrap',
        marginBottom: 10,
    },
    lineInfoItem: {
        marginBottom: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    lineInfoItemIcon: {
        width: 20,
        height: 20,
        padding: 5,
        backgroundColor: 'red',
        borderRadius: 50,
        marginRight: 5,

        justifyContent: 'center',
        alignItems: 'center'
    },
    lineInfoItemText: {
        fontSize: 16,
        fontWeight: 'bold'
    },

    sectionItem: {
        flexDirection: 'row',
        marginBottom: 10
    },
    label: {
        fontWeight: 'bold'
    },
    text: {
        fontSize: 18,
        lineHeight: 20
    },

    bottomContent: {
        paddingBottom: 10
    },

    blockComment: {
        marginBottom: 10,
    },
    blockCommentLabel: {
        fontWeight: 'bold',
        marginBottom: 5
    },
    blockCommentContent: {
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 2
    }
})

export default Application
