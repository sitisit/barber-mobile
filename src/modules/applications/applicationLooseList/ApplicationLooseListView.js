import React, {Component, createRef} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Animated,
    ActivityIndicator,
    TouchableWithoutFeedback,
    SafeAreaView,
    FlatList
} from 'react-native';
import {LoaderApp} from "../../../components/LoaderApp";
import ApplacationCard from '../../../components/ApplicationCard'
import Calendars from '../../../components/Calendars'
import Filter from "../../../components/Filter";
import NotDataBlock from "../../../components/NotDataBlock";
import ModalHideBottom from "../../../components/ModalHideBottom";
import ApplicationPage from "../../../components/ApplicationPage";
import color from "../../../styles/color";
import {Icon} from 'native-base';
import axios from "../../../plugins/axios";
import {DropDownHolder} from "../../../components/DropDownAlert";
import components from "../../../styles/components";

const AnimatedView = Animated.createAnimatedComponent(View);

class ApplicationLooseList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            listView: 'list',
            filter: [],
            list: [],

            application: {},
            pagination: {
                page: 0
            },

            setRelativeViewTypes: true,
            openFilter: false,
            openApplication: false,
            refreshing: false,

            sort: 1,

            topViewTypes: new Animated.Value(10)
        }

        this.filterRef = React.createRef();
        this.applicationRef = React.createRef();
    }

    componentDidMount = () => {
        this.props.navigation.setParams({handleOpenFilter: this.openFilter.bind(this)});
        // this.loadListApplications()

        LoaderApp.isChangeLoaderApp(true)
        this.props.navigation.addListener('didFocus', () => {
            this.loadListApplications()
        });
    }

    wait = (timeout) => {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }

    loadListApplications = () => {
        LoaderApp.isChangeLoaderApp(true)
        let sort = '?sort=' + this.state.sort
        axios('get', 'api/v1/application-feed' + sort).then(response => {
            this.setState({
                list: response.data
            })

            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            console.log(error.response)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    openFilter = () => {
        this.setState({
            openFilter: true
        })
    }
    isFilter = () => {
        if (this.filterRef.current) {
            this.setState({
                filter: this.filterRef.current.state.filter
            })
        }

        this.setState({openFilter: false})
    }

    toApplication = (application) => {
        if (true) {
            this.setState({
                openApplication: true,
                application: application
            })
        } else {
            const {navigate} = this.props.navigation
            navigate('ApplicationLoose', {
                id: application.id
            })
        }
    }
    takeWork = (application) => {
        this.setState({
            openApplication: false
        })

        let body = {
            request_id: application.id
        }
        axios('post', 'api/v1/takerequest', body).then(response => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Заявка принята в работе')
            this.loadListApplications()
            this.props.navigation.navigate('ApplicationMyPage', {
                id: application.id,
                type: 0
            })
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    isShowAllRequest = () => {
        LoaderApp.isChangeLoaderApp(true)

        axios('get', 'api/v1/showall').then(response => {
            this.loadListApplications()
            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Заявки все отображены')
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    isHideRequest = (application) => {
        LoaderApp.isChangeLoaderApp(true)

        let body = {
            request_id: application.id
        }

        axios('post', 'api/v1/hiderequest', body).then(response => {
            this.loadListApplications()
            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Заявка скрыта')
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    isChangeSort = (id) => {
        LoaderApp.isChangeLoaderApp(true)
        this.setState({
            sort: id,
            openFilter: false
        })

        this.wait(1000).then(() => {
            this.loadListApplications()
        })
    }

    isOnEndReached = () => {
        let pagination = this.state.pagination

        pagination.page += 1

        this.setState({pagination})
    }
    _renderListFooterComponent = () => {
        if (true) {
            return null
        }

        return <View style={components.flatLoader}>
            <ActivityIndicator
                color={color.primary}
            />
        </View>
    }

    render() {
        return (
            <View style={styles.page}>
                <View style={styles.viewTop}>
                    <AnimatedView style={styles.viewTipeBlock}>
                        <TouchableOpacity
                            style={styles.viewTipeBlockButton}
                            onPress={() => {
                                this.setState({listView: 'list'})
                            }}
                        >
                            <Text
                                style={[styles.viewTipeBlockButtonText, (this.state.listView == 'list') ? styles.viewTipeBlockButtonTextActive : '']}>Список</Text>
                        </TouchableOpacity>
                        <View style={{height: '100%', width: 1, backgroundColor: 'rgba(0, 0, 0, 0.2)'}}></View>
                        <TouchableOpacity
                            style={styles.viewTipeBlockButton}
                            onPress={() => {
                                this.setState({listView: 'calendar'})
                            }}
                        >
                            <Text
                                style={[styles.viewTipeBlockButtonText, (this.state.listView == 'calendar') ? styles.viewTipeBlockButtonTextActive : '']}>Календарь</Text>
                        </TouchableOpacity>
                    </AnimatedView>
                    <TouchableOpacity style={styles.buttonShowAllRequest} onPress={() => this.isShowAllRequest()}>
                        <Icon
                            name="eye"
                            type="FontAwesome5"
                            style={{color: color.primary, fontSize: 20}}
                        />
                    </TouchableOpacity>
                </View>

                {
                    (this.state.listView == 'list') ? (
                            <SafeAreaView style={{flex: 1}}>
                                <FlatList
                                    data={this.state.list}
                                    extraData={this.state}
                                    keyExtractor={(item, index) => {return item.id + '-' + index}}
                                    renderItem={({item}) => (
                                        <TouchableOpacity onPress={() => this.toApplication(item)}>
                                            <ApplacationCard
                                                data={item}
                                                hideRequest={(application) => this.isHideRequest(application)}
                                            />
                                        </TouchableOpacity>
                                    )}
                                    ListEmptyComponent={() => (
                                        <NotDataBlock
                                            title={'Заявок нет'}
                                        />
                                    )}
                                    showsVerticalScrollIndicator={false}
                                    contentContainerStyle={{padding: 20}}
                                    onRefresh={() => this.loadListApplications()}
                                    refreshing={this.state.refreshing}
                                />
                            </SafeAreaView>
                        )
                        : (
                            <Calendars
                                data={this.state.list}
                                toApplication={(item) => this.toApplication(item)}
                            />
                        )
                }

                <ModalHideBottom
                    ref={this.applicationRef}
                    open={this.state.openApplication}
                    close={() => {
                        this.setState({openApplication: false})
                    }}
                >
                    <ApplicationPage
                        data={this.state.application}
                        takeWork={(application) => this.takeWork(application)}
                    />
                </ModalHideBottom>

                <Filter
                    ref={this.filterRef}
                    open={this.state.openFilter}
                    close={() => {
                        this.setState({openFilter: false})
                    }}
                    isFilter={() => this.isFilter()}
                    isChangeSort={(id) => this.isChangeSort(id)}
                />
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        let params = navigation.state.params
        return {
            headerTitle: 'Лента заявок',
            headerRight: () => (
                (params && params.handleOpenFilter)
                    ?
                    <TouchableOpacity
                        onPress={() => params.handleOpenFilter()}
                        style={styles.filterButton}
                    >
                        <Text style={{
                            textAlign: 'center',
                            fontSize: 12,
                            color: 'white',
                            marginRight: 5
                        }}>Сортировать</Text>
                        <Icon
                            name="sort"
                            type="FontAwesome"
                            style={{
                                color: 'white',
                                fontSize: 22
                            }}
                        />
                    </TouchableOpacity>
                    :
                    <View style={{paddingRight: 20}}>
                        <ActivityIndicator
                            size={'small'}
                            color={'white'}
                        />
                    </View>
            )
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: color.page
    },
    viewTop: {
        flexDirection: 'row',
        paddingBottom: 5,
        zIndex: 2,
        position: 'relative',
        justifyContent: 'space-between',
        width: '100%',
        paddingHorizontal: 15,
        backgroundColor: color.primary
    },
    viewTipeBlock: {
        flexDirection: 'row',
        borderRadius: 3,
        height: 40,
        backgroundColor: '#FFFFFF',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 1,
        elevation: 5,
    },
    viewTipeBlockButton: {
        height: '100%',
        justifyContent: 'center',
        paddingHorizontal: 15,
    },
    viewTipeBlockButtonTextActive: {
        color: '#da7b9e'
    },

    filterButton: {
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonShowAllRequest: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 40,
        height: 40,
        borderRadius: 2,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 1,
        elevation: 5,
    },
})

export default ApplicationLooseList
