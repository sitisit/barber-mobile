// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ApplicationLooseListView from './ApplicationLooseListView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(ApplicationLooseListView);
