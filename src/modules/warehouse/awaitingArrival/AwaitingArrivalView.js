import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity, ActivityIndicator,
} from 'react-native';
import axios from "../../../plugins/axios";
import color from "../../../styles/color";
import {Body, Button, CheckBox, ListItem} from "native-base";
import {DropDownHolder} from "../../../components/DropDownAlert";
import {LoaderApp} from "../../../components/LoaderApp";
import ModalHideBottom from "../../../components/ModalHideBottom";


class AwaitingArrival extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            entryList: [],

            loading: false,
            openModalBottom: false,

            entryProduct: {}
        }
    }

    componentDidMount = () => {
        LoaderApp.isChangeLoaderApp(true)
        this.getList()
        this.props.navigation.addListener('didFocus', () => {
            this.getList()
        });
    }

    getList = () => {
        LoaderApp.isChangeLoaderApp(true)
        axios('get', 'api/v1/warehouse/wait-to-shipment').then(response => {
            response.data.map(item => {
                this.loadListMaterials(item)
                item['materials'] = []
            })
            this.setState({
                list: response.data,
                loading: false
            })
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            LoaderApp.isChangeLoaderApp(false)
        })
    }
    loadListMaterials = (item) => {
        axios('get', 'api/v1/warehouse/take-materials-list?type=2&request_id=' + item.id).then(response => {
            let materials = []

            response.data.list.map(item => {
                materials.push(...item.card)
            })

            item['materials'] = materials
            item['materialsTotalPrice'] = response.data['total_price']

            console.log('this.state.list: ', this.state.list)

            this.setState({
                list: this.state.list
            })
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    entryRequest = (request) => {
        let entryList = this.state.entryList

        if (entryList.indexOf(request.id) == -1) {
            entryList.push(request.id)
        } else {
            entryList.splice(entryList.indexOf(request.id), 1)
        }

        this.setState({entryList})
    }

    receivedMaterials = () => {
        LoaderApp.isChangeLoaderApp(true)
        let requestIds = this.state.entryList.join(',')

        axios('get', 'api/v1/warehouse/take-shipment?request_ids=' + requestIds).then(response => {
            LoaderApp.isChangeLoaderApp(false)
            this.props.navigation.goBack();
        }).catch(error => {
            console.log('error ', error.response)
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    isOpenModalBottom = (item) => {
        this.setState({
            entryProduct: item,
            openModalBottom: true
        })
    }

    render() {
        let {list, loading, viewScreen} = this.state

        if (list.length == 0 && loading) {
            return (
                <View style={[styles.page, styles.pageLoader]}>
                    <ActivityIndicator
                        animating
                        size="large"
                        color={color.primary}
                    />
                </View>
            )
        }
        if (list.length == 0 && !loading) {
            return (
                <View style={[styles.page, styles.pageLoader]}>
                    <Text style={{textAlign: 'center'}}>Нет материалов в ожидание получения</Text>
                </View>
            )
        }

        return (
            <View style={styles.page}>
                <ScrollView
                    style={{flex: 1}}
                    showsVerticalScrollIndicator={false}
                >
                    {
                        list.map((item, idx) => (
                            <ListItem key={'item-' + idx} noIndent onPress={() => this.entryRequest(item)}>
                                <CheckBox checked={this.state.entryList.indexOf(item.id) > -1}
                                          onPress={() => this.entryRequest(item)}/>
                                <Body style={{marginLeft: 5}}>
                                    <Text>{item.want_date} - {item.service_name} ({item.category_name})</Text>
                                    <Button
                                        small
                                        full
                                        style={{marginTop: 10}}
                                        onPress={() => this.isOpenModalBottom(item)}
                                    >
                                        <Text style={{color: 'white'}}>Посмотреть материалы</Text>
                                    </Button>
                                </Body>
                            </ListItem>
                        ))
                    }
                </ScrollView>
                <View style={[styles.bottomContent, (this.state.entryList.length > 0) ? {bottom: 0} : '']}>
                    <Button block onPress={() => this.receivedMaterials()}>
                        <Text style={{color: 'white'}}>Получил</Text>
                    </Button>
                </View>


                <ModalHideBottom
                    open={this.state.openModalBottom}
                    close={() => {
                        this.setState({openModalBottom: false})
                    }}
                >
                    <View
                        style={{padding: 20}}
                    >
                        <Text style={styles.modalTitle}>{this.state.entryProduct.service_name} ({this.state.entryProduct.category_name})</Text>
                        {
                            (this.state.entryProduct.materials && this.state.entryProduct.materials.length > 0) && (
                                <View>
                                    <Text style={{marginBottom: 5, fontSize: 18}}>Материалы на получения:</Text>
                                    {
                                        this.state.entryProduct.materials.map((item, idx) => (
                                            <Text style={{fontSize: 14, marginBottom: 5}}>{ item.name } {item.consumption} {item.unit}</Text>
                                        ))
                                    }
                                </View>
                            )
                        }
                        {
                            ( false && this.state.entryProduct.materialsForWork && this.state.entryProduct.materialsForWork.length > 0) && (
                                <View>
                                    <Text style={{marginBottom: 5, fontSize: 18}}>Материалы для выполнения услуги:</Text>
                                    {
                                        this.state.entryProduct.materials.map((item, idx) => (
                                            <Text style={{fontSize: 14, marginBottom: 5}}>{ item.name } {item.consumption} {item.unit}</Text>
                                        ))
                                    }
                                </View>
                            )
                        }
                        <Text style={{fontSize: 16, fontWeight: 'bold', marginTop: 5}}>Общая стоимость: {this.state.entryProduct.materialsTotalPrice}руб.</Text>
                    </View>
                </ModalHideBottom>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Ожидание получение',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
    pageLoader: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    card: {},

    modalTitle: {
        marginBottom: 10,
        fontSize: 20,
        fontWeight: 'bold'
    },

    bottomContent: {
        position: 'relative',
        paddingBottom: 5,
        bottom: -100
    },
})

export default AwaitingArrival
