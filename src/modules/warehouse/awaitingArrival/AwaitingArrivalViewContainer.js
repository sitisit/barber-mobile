// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import AwaitingArrivalView from './AwaitingArrivalView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(AwaitingArrivalView);
