import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import {Icon, Left, List, ListItem, Right} from "native-base";


class WarehouseList extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount = () => {}

    render() {
        const { navigate } = this.props.navigation

        return (
            <View style={styles.page}>
                <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
                    <List>
                        <ListItem noIndent onPress={() => navigate('Replenishment')}>
                            <Left>
                                <Text>Получить материалы</Text>
                            </Left>
                            <Right>
                                <Icon
                                    name="chevron-right"
                                    type="MaterialIcons"
                                />
                            </Right>
                        </ListItem>
                        <ListItem noIndent onPress={() => navigate('AwaitingArrival')}>
                            <Left>
                                <Text>Ожидание получения</Text>
                            </Left>
                            <Right>
                                <Icon
                                    name="chevron-right"
                                    type="MaterialIcons"
                                />
                            </Right>
                        </ListItem>
                        {
                            (false) && (
                                <ListItem noIndent onPress={() => navigate('ReturnRequest')}>
                                    <Left>
                                        <Text>Вернуть материалы</Text>
                                    </Left>
                                    <Right>
                                        <Icon
                                            name="chevron-right"
                                            type="MaterialIcons"
                                        />
                                    </Right>
                                </ListItem>
                            )
                        }
                        <ListItem noIndent onPress={() => navigate('AwaitingRefund')}>
                            <Left>
                                <Text>Ожидание отправки</Text>
                            </Left>
                            <Right>
                                <Icon
                                    name="chevron-right"
                                    type="MaterialIcons"
                                />
                            </Right>
                        </ListItem>
                        <ListItem noIndent onPress={() => navigate('Warehouse')}>
                            <Left>
                                <Text>Склад</Text>
                            </Left>
                            <Right>
                                <Icon
                                    name="chevron-right"
                                    type="MaterialIcons"
                                />
                            </Right>
                        </ListItem>
                    </List>
                </ScrollView>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Склад',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
})

export default WarehouseList
