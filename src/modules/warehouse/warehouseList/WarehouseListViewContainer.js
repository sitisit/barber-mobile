// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import WarehouseListView from './WarehouseListView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(WarehouseListView);
