import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity, ActivityIndicator, TextInput, KeyboardAvoidingView, Platform,
} from 'react-native';
import color from "../../../styles/color";
import {Body, Button, CheckBox, ListItem, Textarea} from "native-base";
import components from "../../../styles/components";
import {timestampToDate} from "../../../helper/formatting";
import {Select} from "../../../components/Select";
import axios from "../../../plugins/axios";
import {LoaderApp} from "../../../components/LoaderApp";
import {DropDownHolder} from "../../../components/DropDownAlert";
import InputDropdown from "../../../components/InputDropdown";
import axiosDefault from "axios";
import varibles from "../../../varibles";


class ReturnRequest extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            listAddress: [],
            listReturnMaterials: [],

            messageReturnMaterials: '',
            address: '',
            view: '',

            loading: true,
            requestId: 1,
        }
    }

    componentDidMount = () => {
        LoaderApp.isChangeLoaderApp(true)
        this.getList()
        this.props.navigation.addListener('didFocus', () => {
            this.getList()
        });
    }

    getList = () => {
        LoaderApp.isChangeLoaderApp(true)

        axios('get', 'api/v1/warehouse/return-request').then(response => {
            console.log(';response: ', response)
            this.setState({
                loading: false
            })
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            console.log('error.response: ', error.response)

            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            this.setState({
                loading: false
            })
        })
    }
    openList = () => {
        this.setState({
            materialList: [{material: null, count: null}],
            requestId: null
        })
    }
    searchAddress = (search) => {
        this.setState({
            address: search
        })

        axiosDefault({
            method: 'post',
            url: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
            data: {
                "query": search,
                "count": 5,
                "locations": {
                    "city": "Екатеринбург"
                },
                "constraints": {
                    "deletable": true
                },
            },
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Token ${varibles.DADATA_KEY}`
            }
        }).then(response => {
            let listAddress = []

            response.data.suggestions.map((item, idx) => {
                listAddress.push({
                    id: idx,
                    name: item.value
                })
            })

            this.setState({
                listAddress
            })
        }).catch(error => {
            console.log('error: ', error.response)
        })
    }
    returnMaterial = () => {

    }

    render() {
        let {view, list, loading} = this.state

        if (list.length == 0 && loading) {
            return (
                <View style={[styles.page, styles.pageLoader]}></View>
            )
        }
        if (list.length == 0 && !loading) {
            return (
                <View style={[styles.page, styles.pageLoader]}>
                    <Text style={{textAlign: 'center'}}>Нет материалов для возврата</Text>
                </View>
            )
        }

        if (view == 'application-processing') {
            return (
                <View style={styles.page}>
                    <KeyboardAvoidingView
                        behavior={Platform.Os == "ios" ? "padding" : "height"}
                        style={styles.page}
                    >
                        <ScrollView
                            style={{flex: 1}}
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{padding: 20}}
                        >
                            <View>
                                <View style={{marginBottom: 10}}>
                                    <Text style={{fontSize: 14, marginBottom: 5}}>Введите адрес</Text>
                                    <InputDropdown
                                        value={this.state.address}
                                        onChangeText={(search) => this.searchAddress(search)}
                                        list={this.state.listAddress}
                                        onItemSelect={(item) => {
                                            this.setState({address: item.name})
                                        }}
                                        styleInput={[styles.returnMaterialItemInput]}
                                        styleFlatList={{maxHeight: 'auto'}}
                                    />
                                </View>

                                <Text style={{fontSize: 14, marginBottom: 5}}>Комментарий</Text>
                                <Textarea
                                    rowSpan={4}
                                    value={this.state.messageReturnMaterials}
                                    placeholder={'Сообщение...'}
                                    onChangeText={(messageReturnMaterials) => {
                                        this.setState({messageReturnMaterials})
                                    }}
                                    style={[components.textarea, {
                                        marginBottom: 10,
                                        borderWidth: 1,
                                        borderStyle: 'solid',
                                        borderColor: 'rgba(0, 0, 0, 0.4)'
                                    }]}
                                />
                            </View>
                        </ScrollView>
                        <View style={{paddingBottom: 10}}>
                            <Button
                                full
                                primary
                                onPress={() => {
                                    this.setState({openReturnMaterial: true})
                                    this.props.navigation.goBack()
                                }}
                            >
                                <Text style={{color: 'white'}}>Вернуть</Text>
                            </Button>
                        </View>
                    </KeyboardAvoidingView>
                </View>
            )
        }

        return (
            <View style={styles.page}>
                <KeyboardAvoidingView
                    behavior={Platform.Os == "ios" ? "padding" : "height"}
                    style={styles.page}
                >
                    <ScrollView
                        style={{flex: 1}}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{padding: 20}}
                    >

                    </ScrollView>
                </KeyboardAvoidingView>
                <View style={[styles.bottomContent, {bottom: 0}]}>
                    <Button block onPress={() => this.returnMaterial()}>
                        <Text style={{color: 'white'}}>Вернуть</Text>
                    </Button>
                </View>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Вернуть материалы',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
    pageLoader: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    materialCard: {
        flexDirection: 'row',

        padding: 10,
        backgroundColor: 'white',
        borderRadius: 3,
    },

    row: {
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: 'rgba(0, 0, 0, 0.2)'
    },

    bottomContent: {
        paddingBottom: 5
    },

    returnMaterialItemInput: {
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: 'rgba(0, 0, 0, 0.4)',
        borderRadius: 0,
    },
})

export default ReturnRequest
