// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ReturnRequestView from './ReturnRequestView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(ReturnRequestView);
