import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity, ActivityIndicator, TextInput,
} from 'react-native';
import color from "../../../styles/color";
import {LoaderApp} from "../../../components/LoaderApp";
import axios from "../../../plugins/axios";
import {Body, Button, CheckBox, ListItem} from "native-base";
import {timestampToDate} from '../../../helper/formatting'
import {DropDownHolder} from "../../../components/DropDownAlert";
import ModalHideBottom from "../../../components/ModalHideBottom";
import components from "../../../styles/components";


class AwaitingRefund extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            loading: true,
            modalItemOpen: false,

            modalItem: {
                materials: []
            },
        }
    }

    componentDidMount = () => {
        LoaderApp.isChangeLoaderApp(true)
        this.getList()
        this.props.navigation.addListener('didFocus', () => {
            this.getList()
        });
    }

    getList = () => {
        LoaderApp.isChangeLoaderApp(true)

        axios('get', 'api/v1/warehouse/wait-to-return').then(response => {
            let list = []

            response.data.map(item => {
                list.push(item)
            })

            console.log('response: ', response.data)

            this.setState({
                list,
                loading: false
            })
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            this.setState({
                loading: false
            })
            console.log('error:', error.response)
            LoaderApp.isChangeLoaderApp(false)
        })
    }

    changeReturnMaterials = (item) => {
        LoaderApp.isChangeLoaderApp(true)
        let object = {
            request_id: item.id,
            materials: []
        }
        item.materials.map(item => {
            object.materials.push({
                material_id: item.material_id,
                quantity: item.quantity
            })
        })

        axios('post', 'api/v1/warehouse/return-change', object).then(response => {
            this.transferredCourier(item)
        }).catch(error => {
            console.log('error: ', error.response)
            LoaderApp.isChangeLoaderApp(false)
        })
    }
    transferredCourier = (item) => {
        LoaderApp.isChangeLoaderApp(true)
        let params = [
            'id=' + item.id,
        ]
        let materials = []

        axios('get', 'api/v1/warehouse/give-materials?' + params.join('&')).then(response => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('success', 'Успешно')

            this.getList()
            this.setState({
                modalItemOpen: false
            })
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    openModalTransferredCourier = (item) => {
        item.materials.map(material => {
            material['maxCount'] = Number(material.quantity)
        })
        this.setState({
            modalItem: item,
            modalItemOpen: true
        })
    }

    render() {
        let {list, loading, requestId} = this.state

        if (list.length == 0 && loading) {
            return (
                <View style={[styles.page, styles.pageLoader]}></View>
            )
        }
        if (list.length == 0 && !loading) {
            return (
                <View style={[styles.page, styles.pageLoader]}>
                    <Text style={{textAlign: 'center'}}>Нет материалов в ожидании отправки</Text>
                </View>
            )
        }

        return (
            <View style={styles.page}>
                <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false} contentContainerStyle={{padding: 20}}>
                    {
                        list.map((item, idx) => (
                            <View
                                style={styles.card}
                                key={'item-' + idx}
                            >
                                <Text style={styles.cardTitle}>Дата: {timestampToDate(item.date * 1000)}</Text>
                                <Text style={{fontSize: 14, marginBottom: 2}}>Материалы на возврат:</Text>
                                {
                                    ( item.materials ) && (
                                        <View style={{paddingLeft: 10}}>
                                            {
                                                item.materials.map((item, idx) => (
                                                    <View
                                                        key={'item-' + idx}
                                                        style={styles.cardMaterial}
                                                    >
                                                        <View style={{marginRight: 5}}>
                                                            <Text style={{fontSize: 20}}>•</Text>
                                                        </View>
                                                        <Text>
                                                            {item.material_name}
                                                        </Text>
                                                    </View>
                                                ))
                                            }
                                        </View>
                                    )
                                }
                                <Button
                                    style={{marginTop: 10}}
                                    full
                                    primary
                                    onPress={() => this.openModalTransferredCourier(item)}
                                >
                                    <Text style={{color: 'white'}}>Передать курьеру</Text>
                                </Button>
                            </View>
                        ))
                    }
                </ScrollView>


                <ModalHideBottom
                    open={this.state.modalItemOpen}
                    close={() => {
                        this.setState({modalItemOpen: false})
                    }}
                >
                    <View
                        style={{padding: 20}}
                    >
                        {
                            this.state.modalItem.materials.map((item, idx) => (
                                <View style={styles.returnMaterialItem}>
                                    <Text style={{
                                        fontSize: 14,
                                        marginBottom: 5
                                    }}>{item.material_name}</Text>
                                    <TextInput
                                        value={item.quantity}
                                        placeholder={(1 > item.maxCount)? item.maxCount + '': '1'}
                                        keyboardType={'number-pad'}
                                        onChangeText={(returnCount) => {
                                            if (returnCount){
                                                returnCount = parseInt(returnCount.replace(/[^\d]/g, ''))
                                            }
                                            if (returnCount > item.maxCount) {
                                                returnCount = item.maxCount
                                            }

                                            item.quantity = returnCount + ''
                                            this.setState({
                                                modalItem: this.state.modalItem
                                            })
                                        }}
                                        style={[components.input, styles.returnMaterialItemInput]}
                                    />
                                    <Text style={{fontSize: 10}}>Максимальное количество: {item.maxCount}</Text>
                                </View>
                            ))
                        }
                        <Button
                            style={{marginTop: 10}}
                            full
                            primary
                            onPress={() => this.changeReturnMaterials(this.state.modalItem)}
                        >
                            <Text style={{color: 'white'}}>Передать курьеру</Text>
                        </Button>
                    </View>
                </ModalHideBottom>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Ожидание отправки',
        };
    };
}

const styles = StyleSheet.create({
    returnMaterialItem: {
        marginBottom: 10
    },
    returnMaterialItemInput: {
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: 'rgba(0, 0, 0, 0.4)',
        borderRadius: 0,
    },

    page: {
        flex: 1
    },
    pageLoader: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    card: {
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 3,
        marginBottom: 10
    },
    cardTitle: {
        fontSize: 16,
        marginBottom: 5,
        fontWeight: 'bold'
    },

    cardMaterial: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 2
    },
})

export default AwaitingRefund
