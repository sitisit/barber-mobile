// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import AwaitingRefundView from './AwaitingRefundView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(AwaitingRefundView);
