// https://www.npmjs.com/package/react-native-searchable-dropdown

import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    Animated,
    TextInput,
    KeyboardAvoidingView,
    Platform
} from 'react-native';
import {LoaderApp} from "../../../components/LoaderApp";
import axios from "../../../plugins/axios";
import {DropDownHolder} from "../../../components/DropDownAlert";
import color from "../../../styles/color";
import {Body, Button, CheckBox, ListItem} from "native-base";
import DateTimePicker from "@react-native-community/datetimepicker";
import components from "../../../styles/components";
import {timestampToDate} from "../../../helper/formatting";
import {Select} from "../../../components/Select";
import SearchableDropdown from 'react-native-searchable-dropdown';
import axiosDefault from 'axios'
import varibles from '../../../varibles'
import InputDropdown from "../../../components/InputDropdown";
import ModalHideBottom from "../../../components/ModalHideBottom";

class Replenishment extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            entryList: [],
            periodList: [
                {
                    label: '8:00 - 12:00',
                    value: 1
                },
                {
                    label: '12:00 - 16:00',
                    value: 2
                },
                {
                    label: '16:00 - 20:00',
                    value: 3
                },
                {
                    label: '20:00 - 00:00',
                    value: 4
                },
            ],

            date: new Date(),

            period: null,

            entryProduct: {},

            loading: true,
            showDate: false,
            openModalBottom: false,

            viewScreen: 'list',
            address: null,
            master_comment: '',

            listAddress: [],
            selectedItems: [],
        }
    }

    componentDidMount = () => {
        LoaderApp.isChangeLoaderApp(true)
        this.loadList()
        this.props.navigation.addListener('didFocus', () => {
            this.loadList()
        });
    }

    loadListMaterials = (item) => {
        axios('get', 'api/v1/warehouse/take-materials-list?type=2&request_id=' + item.id).then(response => {
            let materials = []

            response.data.list.map(item => {
                materials.push(...item.card)
            })

            item['materials'] = materials
            item['materialsTotalPrice'] = response.data['total_price']

            this.setState({
                list: this.state.list
            })
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    loadListMaterialsType2 = (item) => {
        axios('get', 'api/v1/warehouse/take-materials-list?type=1&request_id=' + item.id).then(response => {
            let materialsForWork = []

            response.data.list.map(item => {
                materialsForWork.push(...item.card)
            })

            item['materialsForWork'] = materialsForWork

            console.log('response: ', response)

            this.setState({
                list: this.state.list
            })
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    loadList = () => {
        LoaderApp.isChangeLoaderApp(true)

        axios('get', 'api/v1/warehouse/get-to-ship').then(response => {
            response.data.map(item => {
                this.loadListMaterials(item)
                this.loadListMaterialsType2(item)
                item['materials'] = []
                item['materialsForWork'] = []
            })

            this.setState({
                list: response.data,
                loading: false
            })
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    entryRequest = (request) => {
        let entryList = this.state.entryList

        if (entryList.indexOf(request.id) == -1) {
            entryList.push(request.id)
        } else {
            entryList.splice(entryList.indexOf(request.id), 1)
        }

        this.setState({entryList})
    }

    validation = (entryList) => {
        let bool = true
        let { date, period, address } = this.state

        if ( entryList.length <= 0 ){
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Выберите заявки')
            bool = false

            return bool
        }
        // if ( !date ){
        //     DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Выберите дату')
        //     bool = false
        //
        //     return bool
        // }
        // if ( !period ){
        //     DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Выберите период')
        //     bool = false
        //
        //     return bool
        // }
        if ( !address ){
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Введите адрес')
            bool = false

            return bool
        }

        return bool
    }
    replenishment = () => {
        let entryList = []

        this.state.list.map(item => {
            entryList.push(item.id)
        })

        if ( this.validation(entryList) ){
            LoaderApp.isChangeLoaderApp(true)

            let data = {
                request_ids: entryList,
                address: this.state.address,
                master_comment: this.state.master_comment
            }

            axios('post', 'api/v1/warehouse/shipment-request', data).then(response => {
                LoaderApp.isChangeLoaderApp(false)
                this.props.navigation.goBack()
                DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Заявки на получение материалов успешно отправлена.')
            }).catch(error => {
                LoaderApp.isChangeLoaderApp(false)
                error = error.response
                DropDownHolder.dropDown.alertWithType('error', error.data.name, error.data.message)
            })
        }
    }

    searchAddress = (search) => {
        this.setState({
            address: search
        })

        axiosDefault({
            method: 'post',
            url: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
            data: {
                "query": search,
                "count": 5,
                "locations": {
                    "city": "Екатеринбург"
                },
                "constraints": {
                    "deletable": true
                },
            },
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Token ${varibles.DADATA_KEY}`
            }
        }).then(response => {
            let listAddress = []

            response.data.suggestions.map((item, idx) => {
                listAddress.push({
                    id: idx,
                    name: item.value
                })
            })

            this.setState({
                listAddress
            })
        }).catch(error => {
            console.log('error: ', error.response)
        })
    }

    isOpenModalBottom = (item) => {
        this.setState({
            entryProduct: item,
            openModalBottom: true
        })
    }

    render() {
        let {list, loading, viewScreen} = this.state

        if (list.length == 0 && loading) {
            return (
                <View style={[styles.page, styles.pageLoader]}>
                </View>
            )
        }
        if (list.length == 0 && !loading) {
            return (
                <View style={[styles.page, styles.pageLoader]}>
                    <Text style={{maxWidth: '70%', textAlign: 'center'}}>Нет заявок на завтра или вы уже оформили доставку.</Text>
                </View>
            )
        }

        if (viewScreen == 'list') {
            return (
                <View style={styles.page}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        {
                            this.state.list.map((item, idx) => (
                                <ListItem key={'item-' + idx} noIndent onPress={() => this.isOpenModalBottom(item)}>
                                    <Text>{timestampToDate(item.want_datetime * 1000)} - {item.service_name} ({item.category_name})</Text>
                                </ListItem>
                            ))
                        }
                    </ScrollView>
                    <View style={styles.bottomContent}>
                        <Button block onPress={() => {
                            this.setState({viewScreen: 'data'})
                        }}>
                            <Text style={{color: 'white'}}>Заказать доставку</Text>
                        </Button>
                    </View>

                    <ModalHideBottom
                        open={this.state.openModalBottom}
                        close={() => {
                            this.setState({openModalBottom: false})
                        }}
                    >
                        <View
                            style={{padding: 20}}
                        >
                            <Text style={styles.modalTitle}>{this.state.entryProduct.service_name} ({this.state.entryProduct.category_name})</Text>
                            {
                                (this.state.entryProduct.materials && this.state.entryProduct.materials.length > 0) && (
                                    <View>
                                        <Text style={{marginBottom: 5, fontSize: 18}}>Материалы на получения:</Text>
                                        {
                                            this.state.entryProduct.materials.map((item, idx) => (
                                                <Text style={{fontSize: 14, marginBottom: 5}}>{ item.name } {item.consumption} {item.unit}</Text>
                                            ))
                                        }
                                    </View>
                                )
                            }
                            {
                                ( false && this.state.entryProduct.materialsForWork && this.state.entryProduct.materialsForWork.length > 0) && (
                                    <View>
                                        <Text style={{marginBottom: 5, fontSize: 18}}>Материалы для выполнения услуги:</Text>
                                        {
                                            this.state.entryProduct.materials.map((item, idx) => (
                                                <Text style={{fontSize: 14, marginBottom: 5}}>{ item.name } {item.consumption} {item.unit}</Text>
                                            ))
                                        }
                                    </View>
                                )
                            }
                            <Text style={{fontSize: 16, fontWeight: 'bold', marginTop: 5}}>Общая стоимость: {this.state.entryProduct.materialsTotalPrice}руб.</Text>
                        </View>
                    </ModalHideBottom>
                </View>
            )
        }

        return (
            <View style={styles.page}>
                <KeyboardAvoidingView
                    behavior={Platform.Os == "ios" ? "padding" : "height"}
                    style={styles.page}
                >
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{padding: 20}}
                    >
                        {
                            (false)&&(
                                <View>
                                    <View style={styles.bodyItem}>
                                        <Text style={styles.bodyItemTitle}>Выберите дату привоза*</Text>
                                        <TouchableOpacity
                                            onPress={() => this.setState({showDate: true})}
                                            style={[styles.row, components.input]}
                                        >
                                            <Text>{timestampToDate(this.state.date)}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.bodyItem}>
                                        <Text style={styles.bodyItemTitle}>Выберите период привоза*</Text>
                                        <Select
                                            data={this.state.periodList}
                                            selectedValue={this.state.period}
                                            style={[styles.row, styles.select, components.input]}
                                            onValueChange={(period, itemIndex) =>
                                                this.setState({period})
                                            }
                                        />
                                    </View>
                                </View>
                            )
                        }
                        <View style={styles.bodyItem}>
                            <Text style={styles.bodyItemTitle}>Введите адрес привоза*</Text>
                            <InputDropdown
                                value={this.state.address}
                                onChangeText={(search) => this.searchAddress(search)}
                                list={this.state.listAddress}
                                onItemSelect={(item) => {
                                    this.setState({address: item.name})
                                }}
                            />
                        </View>
                        <View style={styles.bodyItem}>
                            <Text style={styles.bodyItemTitle}>Комментарий</Text>
                            <TextInput
                                value={this.state.master_comment}
                                onChangeText={master_comment => {
                                    this.setState({master_comment})
                                }}
                                style={[styles.row, styles.select, components.input]}
                                placeholder={'Введите'}
                                multiline
                            />
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
                <View style={styles.bottomContent}>
                    <Button block onPress={() => this.replenishment()}>
                        <Text style={{color: 'white'}}>Заказать</Text>
                    </Button>
                </View>
                {
                    (this.state.showDate) && (
                        <DateTimePicker
                            style={{width: '100%'}}
                            minimumDate={new Date()}
                            value={this.state.date}
                            onChange={(type, date) => {
                                this.setState({
                                    showDate: false,
                                })
                                if (date) {
                                    this.setState({
                                        date
                                    })
                                }
                            }}
                        />
                    )
                }
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Получить материалы',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
    pageLoader: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bodyItem: {
        marginBottom: 10
    },
    bodyItemTitle: {
        fontSize: 16,
        marginBottom: 5
    },
    bottomContent: {
        paddingBottom: 5,
    },

    modalTitle: {
        marginBottom: 10,
        fontSize: 20,
        fontWeight: 'bold'
    },

    searchableDropdownItem: {
        width: '100%',
        padding: 10,
    },
})

export default Replenishment
