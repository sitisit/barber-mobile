// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ReplenishmentView from './ReplenishmentView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(ReplenishmentView);
