import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity, TextInput,
} from 'react-native';
import {LoaderApp} from "../../../components/LoaderApp";
import axios from "../../../plugins/axios";
import {DropDownHolder} from "../../../components/DropDownAlert";
import {Button, Icon, Textarea} from "native-base";
import color from "../../../styles/color";
import components from "../../../styles/components";
import InputDropdown from "../../../components/InputDropdown";
import ModalHideBottom from "../../../components/ModalHideBottom";
import axiosDefault from "axios";
import varibles from "../../../varibles";
import {Select} from "../../../components/Select";


class Warehouse extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            listReturnMaterial: [],
            listAddress: [],
            listReturnRequest: [],

            serviceReturnId: null,

            view: '',
            address: '',
            messageReturnMaterials: '',

            loading: true,
            openReturnMaterial: false,
            showErrorCity: false,
        }
    }

    componentDidMount = () => {
        LoaderApp.isChangeLoaderApp(true)

        this.loadList()
        this.getListReturnRequest()
    }

    loadList = () => {
        axios('get', 'api/v1/warehouse/get-storage').then(response => {
            let list = []
            let listReturnMaterial = []

            response.data.map(item => {
                if (item.return) {
                    listReturnMaterial.push(item)
                }

                list.push(item)
            })

            this.setState({
                list,
                listReturnMaterial,
                loading: false
            })
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            LoaderApp.isChangeLoaderApp(false)
        })
    }

    getListReturnRequest = () => {
        axios('get', '/api/v1/warehouse/get-requestst-to-return').then(response => {
            let list = []

            response.data.map(item => {
                list.push({
                    label: item.name,
                    value: item.request_id
                })
            })

            this.setState({
                listReturnRequest: list
            })
        }).catch(error => {
            console.log(error.response)
        })
    }
    returnValidation = () => {
        let bool = true

        if ( !this.state.address ){
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Введите адрес')
            bool = false

            return bool
        }

        return bool
    }
    returnMaterial = () => {
        if (this.returnValidation()){
            let materials = []

            this.state.listReturnMaterial.map(item => {
                let returnCount = item.returnCount

                if (!returnCount){
                    returnCount = (1 > item.quantity)? item.quantity: 1
                }

                materials.push({
                    material_id: item.material_id,
                    quantity: returnCount + '',
                    material_name: item.material_name
                })
            })

            let data = {
                type: 2,
                address: this.state.address,
                master_comment: this.state.messageReturnMaterials,
                materials: materials
            }

            if(this.state.serviceReturnId != null){
                data['service_id'] = this.state.serviceReturnId
            }

            console.log('data ', data)

            axios('post', 'api/v1/warehouse/return-request', data).then(response => {
                LoaderApp.isChangeLoaderApp(false)
                DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Заявка на возврат оформлена')

                this.setState({
                    openReturnMaterial: false
                })

                this.props.navigation.goBack()
            }).catch(error => {
                LoaderApp.isChangeLoaderApp(false)
                DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            })
        }
    }
    returnMaterialsAll = () => {
        this.state.listReturnMaterial.map(item => {
            item.returnCount = item.quantity + ''
        })

        this.setState({
            listReturnMaterial: this.state.listReturnMaterial
        })

        this.returnMaterial()
    }
    searchAddress = (search) => {
        this.setState({
            address: search
        })

        axiosDefault({
            method: 'post',
            url: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
            data: {
                "query": search,
                "count": 5,
                "locations": {
                    "city": "Екатеринбург"
                },
                "constraints": {
                    "deletable": true
                },
            },
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Token ${varibles.DADATA_KEY}`
            }
        }).then(response => {
            let listAddress = []

            response.data.suggestions.map((item, idx) => {
                listAddress.push({
                    id: idx,
                    name: item.value
                })
            })

            this.setState({
                listAddress
            })
        }).catch(error => {
            console.log('error: ', error.response)
        })
    }

    render() {
        if (this.state.loading) {
            return (<></>)
        }

        if (this.state.list.length <= 0) {
            return (
                <View style={[styles.page, styles.pageLoader]}>
                    <Text>Склад пустой</Text>
                </View>
            )
        }

        if (this.state.view == 'selected-material-return') {
            return (
                <View style={styles.page}>
                    <ScrollView
                        style={{flex: 1}}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{padding: 20}}
                    >
                        {
                            this.state.listReturnMaterial.map((item, idx) => (
                                <View style={styles.returnMaterialItem}>
                                    <Text style={{
                                        fontSize: 14,
                                        marginBottom: 5
                                    }}>{item.material_name}</Text>
                                    <TextInput
                                        value={item.returnCount}
                                        placeholder={(1 > item.quantity)? item.quantity + '': '1'}
                                        keyboardType={'number-pad'}
                                        onChangeText={(returnCount) => {
                                            if (returnCount > item.quantity) {
                                                returnCount = item.quantity + ''
                                            }

                                            item.returnCount = returnCount
                                            this.setState({
                                                listReturnMaterial: this.state.listReturnMaterial
                                            })
                                        }}
                                        style={[components.input, styles.returnMaterialItemInput]}
                                    />
                                    <Text style={{fontSize: 10}}>Максимальное количество: {item.quantity}</Text>
                                </View>
                            ))
                        }

                        <View style={styles.returnMaterialItemInput}>
                            <Select
                                data={this.state.listReturnRequest}
                                selectedValue={this.state.serviceReturnId}
                                placeholder={'Выберите заявку'}
                                style={[styles.returnMaterialItemInput, components.input]}
                                onValueChange={(serviceReturnId, itemIndex) =>
                                    this.setState({serviceReturnId})
                                }
                            />
                        </View>
                        <Text style={{fontSize: 10}}>Выберите услугу по которой хотите вернуть материалы</Text>
                    </ScrollView>
                    <View style={{paddingBottom: 5}}>
                        <Button style={{marginBottom: 5}} full primary onPress={() => {
                            this.setState({openReturnMaterial: true})
                        }}>
                            <Text style={{color: 'white'}}>Вернуть</Text>
                        </Button>
                        <Button
                            full
                            primary
                            onPress={() => {
                                this.setState({view: 'table'})
                            }}
                            style={{backgroundColor: color.error}}
                        >
                            <Text style={{color: 'white'}}>Таблица</Text>
                        </Button>
                    </View>


                    <ModalHideBottom
                        open={this.state.openReturnMaterial}
                        close={() => {
                            this.setState({openReturnMaterial: false})
                        }}
                    >
                        <View style={styles.cancelContent}>
                            <View>
                                <View style={{marginBottom: 10}}>
                                    <Text style={{fontSize: 14, marginBottom: 5}}>Введите адрес</Text>
                                    <InputDropdown
                                        value={this.state.address}
                                        onChangeText={(search) => this.searchAddress(search)}
                                        list={this.state.listAddress}
                                        onItemSelect={(item) => {
                                            this.setState({address: item.name})
                                        }}
                                        styleInput={[styles.returnMaterialItemInput]}
                                        styleFlatList={{maxHeight: 'auto'}}
                                    />
                                </View>

                                <Text style={{fontSize: 14, marginBottom: 5}}>Комментарий</Text>
                                <Textarea
                                    rowSpan={4}
                                    value={this.state.messageReturnMaterials}
                                    placeholder={'Сообщение...'}
                                    onChangeText={(messageReturnMaterials) => {
                                        this.setState({messageReturnMaterials})
                                    }}
                                    style={[components.textarea, {
                                        marginBottom: 10,
                                        borderWidth: 1,
                                        borderStyle: 'solid',
                                        borderColor: 'rgba(0, 0, 0, 0.4)'
                                    }]}
                                />

                                <Button
                                    full
                                    primary
                                    onPress={() => this.returnMaterial()}
                                    style={{marginBottom: 10, marginTop: 10}}
                                >
                                    <Text style={{color: 'white'}}>Вернуть</Text>
                                </Button>

                                <Button
                                    full
                                    primary
                                    onPress={() => this.returnMaterialsAll()}
                                    style={{marginBottom: 10, backgroundColor: color.error}}
                                >
                                    <Text style={{color: 'white'}}>Вернуть все</Text>
                                </Button>
                            </View>
                        </View>
                    </ModalHideBottom>
                </View>
            )
        }

        return (
            <View style={styles.page}>
                <View
                    style={[
                        styles.tableLine,
                        styles.tableLineFixed,
                    ]}
                >
                    <View style={[styles.tableCollumnName, styles.tableCollumItem]}>
                        <Text style={styles.tableText}>Наименование</Text>
                    </View>
                    <View style={[styles.tableCollumnQuantity, styles.tableCollumItem]}>
                        <Text style={styles.tableText}>Кол-во</Text>
                    </View>
                    <View style={[styles.tableCollumnReturn, styles.tableCollumItem, {borderBottomWidth: 0}]}>
                        <Text style={styles.tableText}>Возврат</Text>
                    </View>
                </View>
                <ScrollView
                    style={{flex: 1}}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{paddingTop: 30}}
                >
                    <View style={styles.table}>
                        {
                            this.state.list.map((item, idx) => (
                                <View
                                    key={'row-' + idx}
                                    style={[
                                        styles.tableLine,
                                        ((idx % 2) == 0) ? '' : styles.tableLineDark
                                    ]}
                                >
                                    <View style={[styles.tableCollumnName, styles.tableCollumItem]}>
                                        <Text style={[styles.tableText, styles.tableTextName]}>Наименование</Text>
                                        <Text style={styles.tableText}>{item.material_name}</Text>
                                    </View>
                                    <View style={[styles.tableCollumnQuantity, styles.tableCollumItem]}>
                                        <Text style={[styles.tableText, styles.tableTextName]}>Колличество</Text>
                                        <Text style={styles.tableText}>{item.quantity} {item.unit}</Text>
                                    </View>
                                    <View
                                        style={[styles.tableCollumnReturn, styles.tableCollumItem, {borderBottomWidth: 0}]}>
                                        <Text style={[styles.tableText, styles.tableTextName]}>Возврат</Text>
                                        <View style={{paddingHorizontal: 5}}>
                                            <Icon
                                                name={(item.return) ? 'done' : 'clear'}
                                                type="MaterialIcons"
                                                style={styles.tableIcon}
                                            />
                                        </View>
                                    </View>
                                </View>
                            ))
                        }
                    </View>
                </ScrollView>
                <View style={[styles.bottomContent, (this.state.listReturnMaterial.length > 0) ? {bottom: 0} : '']}>
                    <Button full primary onPress={() => {
                        this.setState({view: 'selected-material-return'})
                    }}>
                        <Text style={{color: 'white'}}>Вернуть материалы</Text>
                    </Button>
                </View>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Склад',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },

    returnMaterialItem: {
        marginBottom: 10
    },

    cancelContent: {
        padding: 20
    },

    returnMaterialItemInput: {
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: 'rgba(0, 0, 0, 0.4)',
        borderRadius: 0,
    },

    tableCollumnName: {
        width: '55%'
    },
    tableCollumnQuantity: {
        width: '25%'
    },
    tableCollumnReturn: {
        width: '20%'
    },

    tableLineFixed: {
        position: 'absolute',
        left: 0,
        top: 0,
        backgroundColor: 'white',
        width: '100%',
        zIndex: 999,
        borderWidth: 0,
        marginBottom: 10,
        height: 30
    },

    table: {},
    tableTextName: {
        display: 'none',
        fontWeight: 'bold',
        borderRightWidth: 1,
        borderStyle: 'solid',
        borderColor: 'rgba(0, 0, 0, 0.2)',
    },
    tableText: {
        fontSize: 12,
        paddingVertical: 5,
        paddingHorizontal: 5,
    },
    tableIcon: {
        fontSize: 15
    },
    tableLine: {
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: '#000'
    },
    tableLineDark: {
        backgroundColor: 'rgba(255, 167, 200, 0.3)'
    },
    tableCollumItem: {
        flexDirection: 'row',
        borderBottomWidth: 0,
        borderStyle: 'solid',
        borderColor: 'rgba(0, 0, 0, 0.2)',
    },

    pageLoader: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    bottomContent: {
        paddingBottom: 5,
        position: 'relative',
        bottom: -1000
    }
})

export default Warehouse
