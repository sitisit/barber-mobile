// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import WarehouseView from './WarehouseView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(WarehouseView);
