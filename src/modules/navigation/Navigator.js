import React, {Component} from 'react';
import AppNavigator from './RootNavigation';
import AppNavigatorAuthorization from '../authorization/RootNavigation'
import {connect} from "react-redux";
import {compose, lifecycle} from "recompose";
import {ActivityIndicator, AsyncStorage, View} from "react-native";
import axios from "../../plugins/axios";
import {setUser} from "../profile/personalArea/PersonalAreaState";
import color from "../../styles/color";

class NavigatorView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoad: false
        }
    }

    componentDidMount = () => {
        this.loadUser()
    }
    loadUser = async () => {
        let token = await AsyncStorage.getItem('token')
        if (token) {
            axios('get', 'api/v1/me').then(response => {
                let user = response.data
                this.props.setUser(user)

                this.setState({isLoad: true})
            }).catch(error => {
                this.setState({isLoad: true})
            })
        }else {
            this.setState({isLoad: true})
        }

    }

    render() {
        if (!this.state.isLoad) {
            return (
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: color.primary
                    }}
                >
                    <ActivityIndicator
                        animating
                        size="large"
                        color="#FFFFFF"
                    />
                </View>
            )
        }

        if (!this.props.user.user) {
            return <AppNavigatorAuthorization/>
        }

        return (
            <AppNavigator/>
        )
    }
}

export default compose(
    connect(
        state => ({
            user: state.user
        }),
        dispatch => ({
            setUser: (user) => dispatch(setUser(user))
        }),
    ),
)(NavigatorView);
