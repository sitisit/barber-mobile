import React from 'react';
import {
    Image,
    View,
    StyleSheet,
    Text,
    StatusBar
} from 'react-native';
import {createBottomTabNavigator, BottomTabBar} from 'react-navigation-tabs';
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';

import ApplicationFeedListScreen from '../my-applications/applicationFeedList/ApplicationFeedListViewContainer'
import ApplicationLooseListScreen from '../applications/applicationLooseList/ApplicationLooseListViewContainer'
import ApplicationScreen from '../applications/application/ApplicationViewContainer'
import ApplicationJobsScreen from '../my-applications/applicationJob/ApplicationJobViewContainer'
import ApplicationCompletedScreen from '../my-applications/applicationCompeted/ApplicationCompetedViewContainer'
import ApplicationCanceledScreen from '../my-applications/applicationCanceled/ApplicationCanceledViewContainer'
import ApplicationMyPageScreen from '../my-applications/applicationMyPage/ApplicationMyPageViewContainer'

//личный кабинет
import InitScreen from '../authorization/init/InitViewContainer'
import PersonalAreaScreen from '../profile/personalArea/PersonalAreaViewContainer'
import BalanceScreen from '../profile/balances/balance/BalanceViewContainer'
import BalancePaymentScreen from '../profile/balances/balancePayments/BalancePaymentsViewContainer'
import BalanceConclusionScreen from '../profile/balances/balanceConclusion/BalanceConclusionViewContainer'
import TimeTableScreen from '../profile/timeTable/TimeTableViewContainer'
import AuthorizationScreen from '../authorization/authorization/AuthorizationViewContainer'
import ContactDetailsScreen from '../profile/contactDetails/ContactDetailsViewContainer'
import AccreditationScreen from '../profile/accreditation/AccreditationViewContainer'
import ChangePhoneNumberScreen from '../profile/changePhoneNumber/ChangePhoneNumberViewContainer'
import SettingFilterApplicationScreen from '../profile/settingFilterApplication/SettingFilterApplicationViewContainer'
import TypeMasterScreen from '../profile/typeMaster/TypeMasterViewContainer'
import VirificationScreen from '../authorization/verification/VerificationViewContainer'
import NotificationScreen from '../profile/notifications/NotificationsViewContainer'
import SettingsTimeTableScreen from '../profile/timeTable/timeTableSettings/TimeTableSettingsViewContainer'

//Склад
import ReplenishmentScreen from '../warehouse/replenishment/ReplenishmentViewContainer'
import ReturnRequestScreen from '../warehouse/returnRequest/ReturnRequestViewContainer'
import WarehouseScreen from '../warehouse/warehouse/WarehouseViewContainer'
import WarehouseListScreen from '../warehouse/warehouseList/WarehouseListViewContainer'
import AwaitingArrivalScreen from '../warehouse/awaitingArrival/AwaitingArrivalViewContainer'
import AwaitingRefundScreen from '../warehouse/awaitingRefund/AwaitingRefundViewContainer'

const iconApplicationFeed = require('../../../assets/icons/checklist.png');
const iconApplicationMy = require('../../../assets/icons/document.png');
const iconUser = require('../../../assets/icons/user.png');
const iconWarehouse = require('../../../assets/icons/real-estate.png');


const TabBarComponent = props => <BottomTabBar {...props} />;

const styles = StyleSheet.create({
    tabBarItemContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 5,
        color: '#939393',
    },
    tabBarIcon: {
        width: 20,
        height: 20,
        tintColor: '#000'
    },
    tabBarIconFocused: {
        tintColor: 'white',
    },
    tabBarTitle: {
        color: '#000',
        fontWeight: '300',
        marginTop: 2,
        fontSize: 12
    },
    tabBarTitleFocused: {
        color: 'white'
    },
});

export default createBottomTabNavigator(
    {
        ApplicationFeed: {
            screen: createStackNavigator({
                    ApplicationLooseList: {
                        screen: ApplicationLooseListScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    ApplicationLoose: {
                        screen: ApplicationScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                },
                {
                    defaultNavigationOptions: {...TransitionPresets.SlideFromRightIOS}
                }
            ),
        },
        ApplicationMy: {
            screen: createStackNavigator({
                    ApplicationFeedList: {
                        screen: ApplicationFeedListScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    ApplicationJobs: {
                        screen: ApplicationJobsScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    ApplicationCompleted: {
                        screen: ApplicationCompletedScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    ApplicationCanceled: {
                        screen: ApplicationCanceledScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    ApplicationMyPage: {
                        screen: ApplicationMyPageScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                },
                {
                    defaultNavigationOptions: {...TransitionPresets.SlideFromRightIOS}
                }
            ),
        },
        WarehouseBlock: {
            screen: createStackNavigator({
                    WarehouseList: {
                        screen: WarehouseListScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    Replenishment: {
                        screen: ReplenishmentScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    ReturnRequest: {
                        screen: ReturnRequestScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    Warehouse: {
                        screen: WarehouseScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    AwaitingArrival: {
                        screen: AwaitingArrivalScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    AwaitingRefund: {
                        screen: AwaitingRefundScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                },
                {
                    defaultNavigationOptions: {...TransitionPresets.SlideFromRightIOS}
                }
            ),
        },
        PersonalArea: {
            screen: createStackNavigator({
                    PersonalArea: {
                        screen: PersonalAreaScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    Balance: {
                        screen: BalanceScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    BalancePayments: {
                        screen: BalancePaymentScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    BalanceConclusion: {
                        screen: BalanceConclusionScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    TimeTable: {
                        screen: TimeTableScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    SettingsTimeTable: {
                        screen: SettingsTimeTableScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    ContactDetails: {
                        screen: ContactDetailsScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    Accreditation: {
                        screen: AccreditationScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    ChangePhoneNumber: {
                        screen: ChangePhoneNumberScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    SettingFilterApplication: {
                        screen: SettingFilterApplicationScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    TypeMaster: {
                        screen: TypeMasterScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                    Notification: {
                        screen: NotificationScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            headerBackTitle: ' '
                        }
                    },
                },
                {
                    defaultNavigationOptions: {...TransitionPresets.SlideFromRightIOS}
                }
            ),
        },
    },
    {
        defaultNavigationOptions: ({navigation, screenProps}) => ({
            tabBarIcon: ({focused}) => {
                const {routeName} = navigation.state;
                let iconSource;

                switch (routeName) {
                    case 'ApplicationFeed':
                        iconSource = iconApplicationMy;
                        title = 'Лента';
                        break;
                    case 'ApplicationMy':
                        iconSource = iconApplicationFeed;
                        title = 'Заявки';
                        break;
                    case 'PersonalArea':
                        iconSource = iconUser;
                        title = 'Профиль';
                        break;
                    case 'WarehouseBlock':
                        iconSource = iconWarehouse;
                        title = 'Склад';
                        break;
                    default:
                        iconSource = iconHome;
                }
                return (
                    <View style={styles.tabBarItemContainer}>
                        <Image
                            resizeMode="contain"
                            source={iconSource}
                            style={[styles.tabBarIcon, focused && styles.tabBarIconFocused]}
                        />
                        <Text style={[styles.tabBarTitle, focused && styles.tabBarTitleFocused]}>{title}</Text>
                    </View>
                );
            },
            headerStyle: {
                backgroundColor: '#da7b9e'
            },
            tabBarComponent: (props) => {
                if (true){
                    return <TabBarComponent {...props}/>
                }

                return null
            },
        }),
        initialRouteName: 'ApplicationFeed',
        tabBarPosition: 'bottom',
        animationEnabled: true,
        swipeEnabled: true,
        tabBarOptions: {
            showIcon: true,
            showLabel: false,
            style: {
                backgroundColor: '#da7b9e',
            },
            labelStyle: {
                color: '#7C7C7C',
                fontSize: 10,
                lineHeight: 12,
                padding: 0,
                margin: 0,
                fontWeight: '300'
            },
        },
    },
);
