import { connect } from 'react-redux';
import {compose, lifecycle} from 'recompose';
import {Platform, UIManager} from 'react-native';

import AppView from './AppView';
import {setUser} from '../modules/profile/personalArea/PersonalAreaState'

export default compose(
    connect(
        state => ({}),
        dispatch => ({
            setUser: (user) => dispatch(setUser(user))
        }),
    ),
    lifecycle({
        componentDidMount() {
            if (Platform.OS === 'android') {
                UIManager.getViewManagerConfig('setLayoutAnimationEnabledExperimental') && UIManager.getViewManagerConfig('setLayoutAnimationEnabledExperimental(true)');
            }
        },
    }),
)(AppView);
