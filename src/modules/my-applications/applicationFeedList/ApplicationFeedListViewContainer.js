// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ApplicationFeedListView from './ApplicationFeedListView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(ApplicationFeedListView);
