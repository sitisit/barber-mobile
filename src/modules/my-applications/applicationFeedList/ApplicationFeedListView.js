import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import { List, ListItem, Left, Right, Icon, Button } from 'native-base';
import color from "../../../styles/color";

class ApplicationFeedList extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount = () => {}

    routerTo = (page) => {
        const { navigate } = this.props.navigation

        navigate(page)
    }

    render() {
        const { navigate } = this.props.navigation

        return (
            <View style={styles.page}>
                <ScrollView>
                    <List>
                        <ListItem noIndent onPress={() => navigate('ApplicationJobs')}>
                            <Left>
                                <Text>Заявки в работе</Text>
                            </Left>
                            <Right>
                                <Icon
                                    name="chevron-right"
                                    type="MaterialIcons"
                                />
                            </Right>
                        </ListItem>
                        <ListItem noIndent onPress={() => navigate('ApplicationCompleted')}>
                            <Left>
                                <Text>Выполненные заявки</Text>
                            </Left>
                            <Right>
                                <Icon
                                    name="chevron-right"
                                    type="MaterialIcons"
                                />
                            </Right>
                        </ListItem>
                        <ListItem noIndent onPress={() => navigate('ApplicationCanceled')}>
                            <Left>
                                <Text>Отмененные заявки</Text>
                            </Left>
                            <Right>
                                <Icon
                                    name="chevron-right"
                                    type="MaterialIcons"
                                />
                            </Right>
                        </ListItem>
                    </List>
                </ScrollView>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Заявки',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: color.page
    },
})

export default ApplicationFeedList
