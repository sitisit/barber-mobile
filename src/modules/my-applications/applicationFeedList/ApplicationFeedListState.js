// @flow
type ApplicationFeedListStateType = {};

type ActionType = {
  type: string,
  payload?: any,
};

export const initialState: ApplicationFeedListStateType = {};

export const ACTION = 'ApplicationFeedListState/ACTION';

export function actionCreator(): ActionType {
  return {
    type: ACTION,
  };
}

export default function ApplicationFeedListStateReducer(state: ApplicationFeedListStateType = initialState, action: ActionType): ApplicationFeedListStateType {
  switch (action.type) {
    case ACTION:
      return {
        ...state,
      };
    default:
      return state;
  }
}
