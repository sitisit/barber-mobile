import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    FlatList,
    SafeAreaView,
} from 'react-native';
import ApplicationCardMini from '../../../components/ApplicationCardMini'
import {LoaderApp} from "../../../components/LoaderApp";
import axios from "../../../plugins/axios";
import {DropDownHolder} from "../../../components/DropDownAlert";
import NotDataBlock from "../../../components/NotDataBlock";


class ApplicationCanceled extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [],

            refreshing: false
        }
    }

    componentDidMount = () => {
        LoaderApp.isChangeLoaderApp(true)
        this.props.navigation.addListener('didFocus', () => {
            this.loadList()
        });
    }
    loadList = () => {
        LoaderApp.isChangeLoaderApp(true)

        axios('get', 'api/v1/canceled-requests').then(response => {
            this.setState({
                list: response.data
            })
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            console.log(error.response)
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    render() {
        return (
            <View style={styles.page}>
                <SafeAreaView style={{flex: 1}}>
                    <FlatList
                        data={this.state.list}
                        renderItem={({item}) => (
                            <ApplicationCardMini
                                data={item}
                            />
                        )}
                        ListEmptyComponent={() => (
                            <NotDataBlock
                                text={'Нет отмененных заявок'}
                            />
                        )}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{padding: 20}}
                        onRefresh={() => this.loadList()}
                        refreshing={this.state.refreshing}
                    />
                </SafeAreaView>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Отмененные заявки',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
})

export default ApplicationCanceled
