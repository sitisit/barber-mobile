// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ApplicationCanceledView from './ApplicationCanceledView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(ApplicationCanceledView);
