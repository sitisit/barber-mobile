// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ApplicationMyPageView from './ApplicationMyPageView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(ApplicationMyPageView);
