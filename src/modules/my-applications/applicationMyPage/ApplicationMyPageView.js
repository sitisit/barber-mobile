import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Image,
    TextInput,
    Dimensions,
    Platform,
    KeyboardAvoidingView,
    RefreshControl
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import {Accordion, Icon, Button, ListItem, Left, Right, Radio, List, Textarea} from 'native-base';
import ModalHideBottom from "../../../components/ModalHideBottom";
import {getPrice, getTimeHours} from "../../../helper/formatting";
import {LoaderApp} from "../../../components/LoaderApp";
import color from "../../../styles/color";
import axios from "../../../plugins/axios";
import components from "../../../styles/components";
import {DropDownHolder} from "../../../components/DropDownAlert";
import {SliderBox} from "react-native-image-slider-box";
import InputDropdown from "../../../components/InputDropdown";
import axiosDefault from "axios";
import varibles from "../../../varibles";

// type 0 - Заявки в работе
// type 1 - Выполненые заявки
// type 2 - Отмененные заявки

const screenWidth = Dimensions.get('window').width

class ApplicationMyPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: null,
            statusRequest: null,
            type: null,
            application: null,

            openMap: false,
            openCancelDialog: false,
            openCancelDialogVersion2: false,
            showErrorCancel: false,
            showErrorMessage: false,
            openReturnMaterial: false,
            refreshing: false,

            address: '',
            messageCancel: '',
            doneMessage: '',
            messageReturnMaterials: '',

            payment: -1,

            images: [],
            listAddress: [],
            materialsForWork: [],


            listReturnMaterials: [],

            testCountReturn: 0,
            totalPriceMaterial: 0,

            client_name: null,
            client_phone: null

        }
    }

    componentDidMount = () => {
        let {id, type} = this.props.navigation.state.params

        this.props.navigation.sendSOS = this.sendSOS.bind()

        this.setState({
            id,
            type
        })

        this.isLoadCard(id)
        this.getStatusRequest(id)
        this.getPermissionAsync();

        this.getMaterialReturn(id)
        this.loadListMaterialsType2(id)


    }

    getMaterialReturn = (id) => {
        axios('get', 'api/v1/warehouse/get-return-materials?request_id=' + id).then(response => {
            let listReturnMaterials = []

            for (let key in response.data) {
                listReturnMaterials.push({
                    name: key,
                    maretials: response.data[key]
                })
            }

            console.log('listReturnMaterials: ', listReturnMaterials)

            this.setState({
                listReturnMaterials: listReturnMaterials
            })
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            LoaderApp.isChangeLoaderApp(false)
        })
    }
    loadListMaterialsType2 = (id) => {
        axios('get', 'api/v1/warehouse/take-materials-list?type=1&request_id=' + id).then(response => {
            let materialsForWork = []
            response.data.list.map(item => {
                materialsForWork.push(...item.card)
            })
            this.setState({
                materialsForWork,
                totalPriceMaterial: response.data.total_price
            })
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    isLoadCard = (id) => {
        LoaderApp.isChangeLoaderApp(true)
        axios('get', 'api/v1/request-view?id=' + id).then(response => {
            let application = response.data

            if (application.options.length > 0) {
                application.options = application.options.split('&amp;amp;amp;lt;br&amp;amp;amp;gt;')
            }
            if (application.subcategories.length > 0) {
                application.subcategories = application.subcategories.split('&amp;amp;amp;lt;br&amp;amp;amp;gt;')
            }
            if (application.paints.length > 0) {
                application.paints = application.paints.split('&amp;amp;amp;lt;br&amp;amp;amp;gt;')
            }

            console.log('application: ', application)

            this.setState({application})
            this.getClientInfo()
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            console.log(error.response)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            LoaderApp.isChangeLoaderApp(false)
        })
    }
    getAccordionData = () => {
        let data = []
        let application = this.state.application

        if (application.options && application.options.length > 0) {
            data.push({
                title: 'Дополнительные услуги',
                list: application.options
            })
        }
        if (application.subcategories && application.subcategories.length > 0) {
            data.push({
                title: 'Дополнительная информация',
                list: application.subcategories
            })
        }
        if (application.paints && application.paints.length > 0) {
            data.push({
                title: 'Краска',
                list: application.paints
            })
        }

        return data
    }
    getStatusRequest = (id) => {
        axios('get', 'api/v1/get-request-status?request_id=' + id).then(response => {
            this.setState({statusRequest: response.data})

            this.props.navigation.setParams({
                status: response.data
            })
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    cancelRequest = () => {
        if (this.state.messageCancel.length > 0) {
            LoaderApp.isChangeLoaderApp(true)

            axios('get', 'api/v1/cancel-request?id=' + this.state.id + '&reason=' + this.state.messageCancel).then(response => {
                this.setState({
                    openCancelDialog: false,
                    messageCancel: ''
                })

                this.props.navigation.goBack()
                LoaderApp.isChangeLoaderApp(false)
                DropDownHolder.dropDown.alertWithType('info', '', response.data)
            }).catch(error => {
                console.log(error.response)
                DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
                LoaderApp.isChangeLoaderApp(false)
            })
        } else {
            this.setState({showErrorCancel: true})

            setTimeout(() => {
                this.setState({showErrorCancel: false})
            }, 3000)
        }
    }
    cancelRequestVersion2 = () => {
        if (this.state.messageCancel.length > 0) {
            LoaderApp.isChangeLoaderApp(true)

            axios('get', 'api/v1/cancel-request?id=' + this.state.id + '&reason=' + this.state.messageCancel).then(response => {
                this.setState({
                    openCancelDialogVersion2: false,
                    messageCancel: ''
                })

                this.props.navigation.goBack()
                LoaderApp.isChangeLoaderApp(false)
                DropDownHolder.dropDown.alertWithType('info', '', response.data)
            }).catch(error => {
                console.log(error.response)
                DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
                LoaderApp.isChangeLoaderApp(false)
            })
        } else {
            this.setState({showErrorCancel: true})

            setTimeout(() => {
                this.setState({showErrorCancel: false})
            }, 3000)
        }
    }

    doneRequest = () => {
        LoaderApp.isChangeLoaderApp(true)
        let body = {
            request_id: this.state.id,
            comment: this.state.doneMessage,
        }
        let images = []

        // if ( !this.state.doneMessage ){
        //
        //     return false
        // }

        if (this.state.images.length > 0){
            this.state.images.map(item => {
                images.push(item.base64)
            })

            body['images'] = images
        }else{
            this.setState({
                openFinishService: false
            })
            this.doneRequestEnd()

            return null
        }

        axios('post', 'api/v1/take-done-images', body).then(response => {
            this.setState({
                openFinishService: false
            })
            this.doneRequestEnd()
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)

            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    doneRequestEnd = () => {
        axios('get', 'api/v1/done-request?id=' + this.state.id).then(response => {
            LoaderApp.isChangeLoaderApp(false)

            if (this.state.listReturnMaterials && this.state.listReturnMaterials.length > 0) {
                this.getStatusRequest(this.state.id)
                this.setState({
                    openReturnMaterial: true
                })
            } else {
                this.props.navigation.goBack()
            }
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    getDataClient = () => {
        LoaderApp.isChangeLoaderApp(true)
        axios('get', 'api/v1/view-client?id=' + this.state.id).then(response => {
            this.isLoadCard(this.state.id)
            this.getStatusRequest(this.state.id)
            this.setState({
                client_name: response.data.name,
                client_phone: response.data.phone
            })
        }).catch(error => {
            console.log('error ', error.response)
            LoaderApp.isChangeLoaderApp(false)
            if (error.response.status == 400) {
                DropDownHolder.dropDown.alertWithType('info', 'Предупреждение', error.response.data.message)
            } else {
                DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            }
        })
    }

    getClientInfo = () => {
        axios('get', 'api/v1/get-client-data?id=' + this.state.id).then(response => {
            let status = this.state.statusRequest

            if (status == 3) {
                this.setState({
                    client_name: response.data.name,
                    client_phone: response.data.phone
                })
            }
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    getSupplies = () => {
    }
    returnConsumables = () => {
    }

    returnMaterials = () => {
        LoaderApp.isChangeLoaderApp(true)
        let materials = []

        this.state.listReturnMaterials.map(item => {
            item.maretials.map(item => {
                materials.push({
                    material_id: item.material_id,
                    material_name: item.name,
                    quantity: (item.returnCount) ? item.returnCount : 0
                })
            })
        })

        let data = {
            type: 1,
            request_id: this.state.id,
            address: this.state.address,
            master_comment: this.state.messageReturnMaterials,
            materials: materials
        }

        axios('post', 'api/v1/warehouse/return-request', data).then(response => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Заявка на возврат оформлена')

            this.setState({
                openReturnMaterial: false
            })

            this.props.navigation.goBack()
        }).catch(error => {
            console.log(error.response)

            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })

        // this.setState({
        //     openReturnMaterial: false
        // })
    }

    searchAddress = (search) => {
        this.setState({
            address: search
        })

        axiosDefault({
            method: 'post',
            url: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
            data: {
                "query": search,
                "count": 5,
                "locations": {
                    "city": "Екатеринбург"
                },
                "constraints": {
                    "deletable": true
                },
            },
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Token ${varibles.DADATA_KEY}`
            }
        }).then(response => {
            let listAddress = []

            response.data.suggestions.map((item, idx) => {
                listAddress.push({
                    id: idx,
                    name: item.value
                })
            })

            this.setState({
                listAddress
            })
        }).catch(error => {
            console.log('error: ', error.response)
        })
    }

    getApplicationAddress = (application) => {
        let status = this.state.statusRequest

        if (status == 1 || status == 6 || status == 5) {
            return application.location + ' дом ' + application.house_number
        }
        if (status == 3) {
            return application.location + ' дом ' + application.house_number + ' квартира ' + application.apartment_number
        }
    }

    getMasterPrice = (application) => {
        let status = this.state.statusRequest
        if (status == 6) {
            return application.master_price
        }
        else {
            return application.master_total_price
        }
    }

    getPaymentTypeIcon = (application) => {
        if (application.payment_id == 1) {
            return 'money-bill-alt'
        }
        else if (application.payment_id == 2) {
            return 'credit-card'
        }
        else if (application.payment_id == 3) {
            return 'globe'
        }
        else {
            return 'wallet'
        }
    }

    sendSOS = () => {
        console.log('sos SOS sos')
        LoaderApp.isChangeLoaderApp(true)

        setTimeout(() => {
            LoaderApp.isChangeLoaderApp(false)

            DropDownHolder.dropDown.alertWithType('info', 'Безопасность', 'Заявка была отправлена')

        }, 1000)
    }

    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
            }
        }
    }
    _pickImage = async () => {
        let image = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: false,
            base64: true,
            quality: 0.2
        });

        if (!image.cancelled) {
            let images = this.state.images

            images.push(image)

            this.setState({images});
        }
    };

    _renderAccordionHeader(item, expanded) {
        return (
            <View style={styles.accordionHeader}>
                <Text style={{fontWeight: "600"}}>
                    {item.title}
                </Text>
                {expanded
                    ? <Icon style={{fontSize: 18}} name="remove-circle"/>
                    : <Icon style={{fontSize: 18}} name="add-circle"/>}
            </View>
        );
    }

    _renderAccordionContent(item) {
        return (
            <View style={styles.accordionContent}>
                {
                    item.list.map((item, idx) => (
                        <Text
                            key={item}
                            style={styles.accordionContentText}
                        >
                            {item}
                        </Text>
                    ))
                }
            </View>
        );
    }

    _renderBottomContent = () => {
        let status = this.state.statusRequest

        if (status == 1) {
            return (
                <View style={styles.bottomContent}>
                    <View style={{width: '100%', paddingLeft: 5, marginBottom: 5}}>
                        <Button
                            full
                            primary
                            onPress={() => {
                                this.setState({openCancelDialog: true})
                            }}
                            style={{backgroundColor: color.error, marginBottom: 5}}
                        >
                            <Text style={{color: 'white'}}>Отказаться от заявки</Text>
                        </Button>

                        <Button full primary onPress={() => { this.props.navigation.navigate('Replenishment') }}>
                            <Text style={{color: 'white'}}>Получить материалы</Text>
                        </Button>
                    </View>
                </View>
            )
        }
        if (status == 6 || status == 5) {
            return (
                <View style={styles.bottomContent}>
                    <View style={{width: '100%', paddingLeft: 5, marginBottom: 5}}>
                        <Button
                            full
                            primary
                            onPress={() => {
                                this.setState({openCancelDialogVersion2: true})
                            }}
                            style={{backgroundColor: color.error, marginBottom: 5}}
                        >
                            <Text style={{color: 'white'}}>Отказаться от заявки</Text>
                        </Button>
                        <Button full primary onPress={() => this.getDataClient()}>
                            <Text style={{color: 'white'}}>Получить данные клиента</Text>
                        </Button>
                    </View>
                </View>
            )
        }
        if (status == 3) {
            return (
                <View style={styles.bottomContent}>
                    <View style={{width: '100%', paddingLeft: 5, marginBottom: 5}}>
                        <Button
                            full
                            primary
                            onPress={() => {
                                this.setState({openCancelDialogVersion2: true})
                            }}
                            style={{backgroundColor: color.error, marginBottom: 5}}
                        >
                            <Text style={{color: 'white'}}>Отказаться от заявки</Text>
                        </Button>
                        <Button full primary onPress={() => {
                            this.setState({openFinishService: true})
                        }}>
                            <Text style={{color: 'white'}}>Выполнил</Text>
                        </Button>
                    </View>
                </View>
            )
        }
    }

    render() {
        const application = this.state.application
        if (!application) {
            return <View></View>
        }

        // const accordionData = this.getAccordionData()

        console.log('application: ', application)

        return (
            <View style={styles.page}>
                <KeyboardAvoidingView
                    behavior={Platform.Os == "ios" ? "padding" : "height"}
                    style={styles.page}
                >
                    <ScrollView
                        style={{flex: 1}}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{padding: 20}}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={() => this.isLoadCard(this.state.id)}
                                colors={[color.primary]}
                                tintColor={color.primary}
                            />
                        }
                    >
                        <View style={styles.top}>
                            <Text style={styles.name}>{application.service_name}</Text>
                            <Text style={styles.category}>{application.category_name}</Text>
                        </View>
                        <View style={styles.lineInfo}>
                            <View style={styles.lineInfoItem}>
                                <View style={[styles.lineInfoItemIcon, {backgroundColor: '#308c65'}]}>
                                    <Icon
                                        name="ruble-sign"
                                        type="FontAwesome5"
                                        style={{color: 'white', fontSize: 10}}
                                    />
                                </View>
                                <Text style={styles.lineInfoItemText}>
                                    {
                                        getPrice(this.getMasterPrice(application))
                                    } - вознаграждение мастера
                                </Text>
                            </View>
                            {
                                (false) && (
                                    <View style={styles.lineInfoItem}>
                                        <View style={[styles.lineInfoItemIcon, {backgroundColor: '#308c65'}]}>
                                            <Icon
                                                name="ruble-sign"
                                                type="FontAwesome5"
                                                style={{color: 'white', fontSize: 10}}
                                            />
                                        </View>
                                        <Text style={styles.lineInfoItemText}>
                                            {getPrice(this.state.totalPriceMaterial)} - стоимость материалов
                                        </Text>
                                    </View>
                                )
                            }
                            {
                                (this.state.statusRequest == 3) && (
                                    <View style={styles.lineInfoItem}>
                                        <View style={[styles.lineInfoItemIcon, {backgroundColor: '#308c65'}]}>
                                            <Icon
                                                name="ruble-sign"
                                                type="FontAwesome5"
                                                style={{color: 'white', fontSize: 10}}
                                            />
                                        </View>
                                        <Text style={styles.lineInfoItemText}>
                                            {getPrice(application.total_price)} - стоимость для клиента
                                        </Text>
                                    </View>
                                )
                            }
                            {
                                (this.state.statusRequest == 3) && (
                                    <View style={styles.lineInfoItem}>
                                        <View style={[styles.lineInfoItemIcon, {backgroundColor: '#c90202'}]}>
                                            <Icon
                                                name={ this.getPaymentTypeIcon(application) }
                                                type="FontAwesome5"
                                                style={{color: 'white', fontSize: 10}}
                                            />
                                        </View>
                                        <Text style={styles.lineInfoItemText}>
                                            { application.payment} - тип оплаты                                      </Text>
                                    </View>
                                )
                            }
                            <View style={styles.lineInfoItem}>
                                <View style={[styles.lineInfoItemIcon, {backgroundColor: '#0a6c82'}]}>
                                    <Icon
                                        name="clock"
                                        type="FontAwesome5"
                                        style={{color: 'white', fontSize: 10}}
                                    />
                                </View>
                                <Text style={styles.lineInfoItemText}>~ {getTimeHours(application.lead_time)} - время
                                    выполнения заявки</Text>
                            </View>
                        </View>
                        <View style={styles.sectionItem}>
                            <Text style={styles.text}>
                                <Text style={styles.label}>Дата и время ожидание: </Text>
                                {application.want_date} {application.want_time}
                            </Text>
                        </View>
                        <View style={[styles.sectionItem, {flexDirection: 'column'}]}>
                            <Text style={[styles.text, {marginBottom: 10}]}>
                                <Text style={styles.label}>Адрес: </Text>
                                {
                                    this.getApplicationAddress(application)
                                }
                            </Text>
                            {
                                (this.state.statusRequest == 3) && (
                                    <Text style={[styles.text, {marginBottom: 10}]}>
                                        <Text style={styles.label}>Имя клиента: </Text>
                                        { this.state.client_name }
                                    </Text>
                                )
                            }
                            {
                                (this.state.statusRequest == 3) && (
                                    <Text style={[styles.text, {marginBottom: 10}]}>
                                        <Text style={styles.label}>Телефон: </Text>
                                        { this.state.client_phone }
                                    </Text>
                                )
                            }
                            {
                                (false) && (
                                    <Button bordered full primary small onPress={() => this.setState({openMap: true})}>
                                        <Text style={{color: '#da7b9e'}}>Показать на карте</Text>
                                    </Button>
                                )
                            }
                        </View>

                        <View style={styles.sectionItem}>
                            <Text style={styles.text}>
                                <Text style={styles.label}>Комментарий заказчика: </Text>
                                {application.user_comment}
                            </Text>
                        </View>

                        {
                            (application.subcategories.length > 0) && (
                                <View style={styles.sectionItem}>
                                    <Text style={styles.text}>
                                        <Text style={styles.label}>Длина волос: </Text>
                                        {application.subcategories.join(' ')}
                                    </Text>
                                </View>
                            )
                        }

                        {
                            (application.options.length > 0) && (
                                <View style={styles.sectionItem}>
                                    <Text style={styles.text}>
                                        <Text style={styles.label}>Дополнительные работы: </Text>
                                        {application.options.join(', ')}
                                    </Text>
                                </View>
                            )
                        }

                        {
                            (application.paints.length > 0) && (
                                <View style={styles.sectionItem}>
                                    <Text style={styles.text}>
                                        <Text style={styles.label}>Краска: </Text>
                                        {application.paints.join(', ')}
                                    </Text>
                                </View>
                            )
                        }

                        {
                            (this.state.materialsForWork.length > 0) && (
                                <View>
                                    <Text style={{marginBottom: 5, fontSize: 18}}>Материалы для выполнения
                                        услуги:</Text>
                                    {
                                        this.state.materialsForWork.map((item, idx) => (
                                            <Text style={{
                                                fontSize: 14,
                                                marginBottom: 5
                                            }}>{item.name} {item.consumption} {item.unit}</Text>
                                        ))
                                    }
                                </View>
                            )
                        }

                        {
                            ( false ) && (
                                <View style={{marginBottom: 20}}>
                                    {
                                        (accordionData.length > 0) && (
                                            <Accordion
                                                dataArray={accordionData}
                                                animation={true}
                                                expanded={true}
                                                renderHeader={this._renderAccordionHeader}
                                                renderContent={this._renderAccordionContent}
                                            />
                                        )
                                    }
                                </View>
                            )
                        }

                        <View>
                            {
                                (application.user_images.length > 0) && (
                                    <View style={styles.imagesContainer}>
                                        <Text style={[styles.text, styles.label, {marginBottom: 10}]}>Изображение
                                            пользователя</Text>
                                        <SliderBox
                                            parentWidth={screenWidth - 40}
                                            dotColor={color.primary}
                                            images={application.user_images}
                                        />
                                    </View>
                                )
                            }
                        </View>

                    </ScrollView>
                    {this._renderBottomContent()}

                    <ModalHideBottom
                        open={this.state.openCancelDialog}
                        close={() => {
                            this.setState({openCancelDialog: false})
                        }}
                    >
                        <View style={styles.cancelContent}>
                            <Text style={styles.cancelContentLabel}>Причина отказа заявки</Text>
                            <View>
                                <TextInput
                                    value={this.state.messageCancel}
                                    placeholder={'Введите причину отказа...'}
                                    onChangeText={(messageCancel) => {
                                        this.setState({messageCancel})
                                    }}
                                    style={[components.input, styles.cancelContentInput]}
                                    multiline
                                />
                                {
                                    (this.state.showErrorCancel) ?
                                        <Text style={styles.cancelContentLabelError}>Необходимо заполнить причину
                                            отказа</Text> : <View/>
                                }
                            </View>
                            <Button full primary onPress={() => this.cancelRequest()}
                                    style={{backgroundColor: color.error}}>
                                <Text style={{color: 'white'}}>Отказаться</Text>
                            </Button>
                        </View>
                    </ModalHideBottom>

                    <ModalHideBottom
                        open={this.state.openCancelDialogVersion2}
                        close={() => {
                            this.setState({openCancelDialogVersion2: false})
                        }}
                    >
                        <View style={styles.cancelContent}>
                            <Text style={styles.cancelContentLabel}>Свяжитесь с оператором для отмены заявки</Text>
                        </View>
                    </ModalHideBottom>

                    <ModalHideBottom
                        open={this.state.openFinishService}
                        close={() => {
                            this.setState({openFinishService: false})
                        }}
                    >
                        <View style={styles.cancelContent}>
                            <Text style={styles.cancelContentLabel}>Выберите изображение</Text>
                            <View style={{marginBottom: 10, marginLeft: -10, flexDirection: 'row', flexWrap: 'wrap'}}>
                                {
                                    this.state.images.map((item, idx) => (
                                        <Image
                                            style={{width: 40, height: 40, marginLeft: 10, marginBottom: 10}}
                                            source={{uri: item.uri}}
                                        />
                                    ))
                                }
                                {
                                    (this.state.images.length < 4) && (
                                        <TouchableOpacity
                                            style={{
                                                width: 40,
                                                height: 40,
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                backgroundColor: color.primary,
                                                marginLeft: 10,
                                                marginBottom: 10
                                            }}
                                            onPress={() => this._pickImage()}
                                        >
                                            <Icon
                                                style={{fontSize: 30, color: 'white'}}
                                                name="images"
                                            />
                                        </TouchableOpacity>
                                    )
                                }
                            </View>

                            <Text style={styles.cancelContentLabel}>Сообщение</Text>
                            <View>
                                <TextInput
                                    value={this.state.doneMessage}
                                    placeholder={'Сообщение...'}
                                    onChangeText={(doneMessage) => {
                                        this.setState({doneMessage})
                                    }}
                                    style={[components.input, styles.cancelContentInput]}
                                    multiline
                                />
                                {
                                    (this.state.showErrorCancel) ?
                                        <Text style={styles.cancelContentLabelError}>Необходимо заполнить причину
                                            отказа</Text> : <View/>
                                }
                            </View>
                            {
                                (this.state.showErrorMessage) && (
                                    <Text>Введите сообщение</Text>
                                )
                            }

                            <Button style={{marginTop: 10}} full primary onPress={() => this.doneRequest()}>
                                <Text style={{color: 'white'}}>Выполнил</Text>
                            </Button>
                        </View>
                    </ModalHideBottom>

                    <ModalHideBottom
                        open={this.state.openReturnMaterial}
                        close={() => {
                            this.setState({openReturnMaterial: false})
                        }}
                    >
                        <View style={styles.cancelContent}>
                            <Text style={styles.cancelContentLabel}>Вернуть материалы</Text>

                            {
                                (this.state.listReturnMaterials.length > 0) && (
                                    <View>
                                        {
                                            this.state.listReturnMaterials.map((item, idx) => (
                                                <View>
                                                    <Text style={{fontSize: 16, marginBottom: 5}}>{item.name}</Text>
                                                    {
                                                        item.maretials.map((item, idx) => (
                                                            <View style={styles.returnMaterialItem}>
                                                                <Text style={{
                                                                    fontSize: 14,
                                                                    marginBottom: 5
                                                                }}>{item.name}</Text>
                                                                <TextInput
                                                                    value={item.returnCount}
                                                                    placeholder={'0'}
                                                                    keyboardType={'number-pad'}
                                                                    onChangeText={(returnCount) => {
                                                                        if (returnCount > item.consumption) {
                                                                            returnCount = item.consumption + ''
                                                                        }

                                                                        item.returnCount = returnCount
                                                                        this.setState({
                                                                            listReturnMaterials: this.state.listReturnMaterials
                                                                        })
                                                                    }}
                                                                    style={[components.input, styles.returnMaterialItemInput]}
                                                                    multiline
                                                                />
                                                                <Text style={{fontSize: 10}}>Максимальное
                                                                    количество: {item.consumption}</Text>
                                                            </View>
                                                        ))
                                                    }
                                                </View>
                                            ))
                                        }

                                        <View style={{marginBottom: 10}}>
                                            <Text style={{fontSize: 14, marginBottom: 5}}>Введите адрес</Text>
                                            <InputDropdown
                                                value={this.state.address}
                                                onChangeText={(search) => this.searchAddress(search)}
                                                list={this.state.listAddress}
                                                onItemSelect={(item) => {
                                                    this.setState({address: item.name})
                                                }}
                                                styleInput={[styles.returnMaterialItemInput]}
                                                styleFlatList={{maxHeight: 'auto'}}
                                            />
                                        </View>

                                        <Text style={{fontSize: 14, marginBottom: 5}}>Комментарий</Text>
                                        <Textarea
                                            rowSpan={4}
                                            value={this.state.messageReturnMaterials}
                                            placeholder={'Сообщение...'}
                                            onChangeText={(messageReturnMaterials) => {
                                                this.setState({messageReturnMaterials})
                                            }}
                                            style={[components.textarea, {
                                                marginBottom: 10,
                                                borderWidth: 1,
                                                borderStyle: 'solid',
                                                borderColor: 'rgba(0, 0, 0, 0.4)'
                                            }]}
                                        />

                                        <Button
                                            full
                                            primary
                                            onPress={() => this.returnMaterials()}
                                            style={{marginBottom: 10, marginTop: 10}}
                                        >
                                            <Text style={{color: 'white'}}>Вернуть</Text>
                                        </Button>

                                        <Button
                                            full
                                            primary
                                            onPress={() => this.returnMaterials()}
                                            style={{marginBottom: 10, backgroundColor: color.error}}
                                        >
                                            <Text style={{color: 'white'}}>Все израсходовал</Text>
                                        </Button>
                                    </View>
                                )
                            }

                            <Button
                                full
                                primary
                                style={{backgroundColor: color.error}}
                                onPress={() => {
                                    this.setState({openReturnMaterial: true})
                                    this.props.navigation.goBack()
                                }}
                            >
                                <Text style={{color: 'white'}}>Вернуть позже</Text>
                            </Button>
                        </View>
                    </ModalHideBottom>
                </KeyboardAvoidingView>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Заявка',
            headerRight: () => (
                (navigation.state.params.status == 3) && (
                    <TouchableOpacity
                        style={styles.buttonHeaderHelp}
                        onPress={() => navigation.sendSOS()}
                    >
                        <Icon
                            name="record-voice-over"
                            type="MaterialIcons"
                            style={styles.buttonHeaderHelpIcon}
                        />
                        <Text style={styles.buttonHeaderHelpText}>ТРЕВОГА</Text>
                    </TouchableOpacity>
                )
            )
        };
    };
}

const styles = StyleSheet.create({
    buttonHeaderHelp: {
        marginRight: 15,
        width: 60,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ff0000',
        borderRadius: 20
    },
    buttonHeaderHelpIcon: {
        fontSize: 16,
        color: 'white'
    },
    buttonHeaderHelpText: {
        fontSize: 12,
        textAlign: 'center',
        color: 'white'
    },

    page: {
        flex: 1,
        backgroundColor: color.page
    },

    returnMaterialItem: {
        marginBottom: 5
    },
    returnMaterialItemInput: {
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: 'rgba(0, 0, 0, 0.4)',
        borderRadius: 0,
    },

    top: {
        marginBottom: 10
    },
    name: {
        fontSize: 18,
        lineHeight: 20,
        fontWeight: 'bold'
    },
    category: {
        fontSize: 20,
        lineHeight: 22
    },

    imageContainer: {
        width: '100%',
        height: 200,
        marginBottom: 15
    },
    image: {
        flex: 1
    },

    lineInfo: {
        flexWrap: 'wrap',
        marginBottom: 10,
    },
    lineInfoItem: {
        marginBottom: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    lineInfoItemIcon: {
        width: 20,
        height: 20,
        padding: 5,
        backgroundColor: 'red',
        borderRadius: 50,
        marginRight: 5,

        justifyContent: 'center',
        alignItems: 'center'
    },
    lineInfoItemText: {
        fontSize: 16,
        fontWeight: 'bold',
        flex: 1
    },

    sectionItem: {
        flexDirection: 'row',
        marginBottom: 10
    },
    label: {
        fontWeight: 'bold'
    },
    text: {
        fontSize: 18,
        lineHeight: 20
    },

    accordionHeader: {
        flexDirection: "row",
        padding: 10,
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: "white",
        borderTopWidth: 1,
        borderStyle: 'solid',
        borderColor: 'black'
    },
    accordionContent: {
        padding: 10,
        backgroundColor: 'white',
        borderStyle: 'solid',
        borderColor: 'black'
    },
    accordionContentText: {},

    imagesContainer: {},

    cancelContent: {
        padding: 20
    },
    cancelContentLabel: {
        fontSize: 18,
        marginBottom: 10
    },
    cancelContentInput: {
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: 'rgba(0, 0, 0, 0.4)',
        borderRadius: 0,

        marginBottom: 20
    },
    cancelContentLabelError: {
        position: 'absolute',
        bottom: 0,
        color: color.primary
    },

    bottomContent: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginLeft: -5
    }
})

export default ApplicationMyPage
