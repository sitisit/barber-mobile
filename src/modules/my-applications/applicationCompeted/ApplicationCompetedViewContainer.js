// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ApplicationCompetedView from './ApplicationCompetedView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(ApplicationCompetedView);
