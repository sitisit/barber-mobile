import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity, FlatList, SafeAreaView, ActivityIndicator,
} from 'react-native';
import ApplacationCard from "../../../components/ApplicationCard";
import NotDataBlock from "../../../components/NotDataBlock";
import axios from "../../../plugins/axios";
import components from "../../../styles/components";
import color from "../../../styles/color";
import {LoaderApp} from "../../../components/LoaderApp";
import {DropDownHolder} from "../../../components/DropDownAlert";


class ApplicationCompeted extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [],

            pagination: {
                page: 1
            },

            refreshing: false
        }
    }

    componentDidMount = () => {
        LoaderApp.isChangeLoaderApp(true)
        this.props.navigation.addListener('didFocus', () => {
            this.loadList()
        });
    }

    loadList = () => {
        LoaderApp.isChangeLoaderApp(true)
        axios('get', 'api/v1/completed-requests').then(response => {
            this.setState({
                list: response.data
            })
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    isOnEndReached = () => {
        let pagination = this.state.pagination

        pagination.page += 1

        this.setState({pagination})
    }
    _renderListFooterComponent = () => {
        if (true){
            return null
        }

        return <View style={components.flatLoader}>
            <ActivityIndicator
                color={color.primary}
            />
        </View>
    }

    render() {
        return (
            <View style={styles.page}>
                <SafeAreaView style={{flex: 1}}>
                    <FlatList
                        ref={this.refScrollView}
                        data={this.state.list}
                        renderItem={({item}) => (
                            <ApplacationCard data={item}/>
                        )}
                        ListEmptyComponent={() => (
                            <NotDataBlock
                                text={'Нет выполненых заявок'}
                            />
                        )}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{padding: 20}}
                        onRefresh={() => this.loadList()}
                        refreshing={this.state.refreshing}

                        onEndReached={() => this.isOnEndReached()}
                        ListFooterComponent={() => this._renderListFooterComponent()}
                    />
                </SafeAreaView>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Выполненные заявки',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
})

export default ApplicationCompeted
