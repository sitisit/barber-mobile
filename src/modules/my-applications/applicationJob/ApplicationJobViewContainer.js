// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ApplicationJobView from './ApplicationJobView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(ApplicationJobView);
