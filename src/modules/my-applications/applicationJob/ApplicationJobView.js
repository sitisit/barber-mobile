import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    SafeAreaView,
    FlatList
} from 'react-native';
import ApplacationCard from "../../../components/ApplicationCardJob.js";
import NotDataBlock from "../../../components/NotDataBlock";
import {LoaderApp} from "../../../components/LoaderApp";
import axios from "../../../plugins/axios";
import {DropDownHolder} from "../../../components/DropDownAlert";


class ApplicationJob extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [],

            refreshing: false
        }
    }

    componentDidMount = () => {
        LoaderApp.isChangeLoaderApp(true)
        this.props.navigation.addListener('didFocus', () => {
            this.loadList()
        });
    }

    loadList = () => {
        LoaderApp.isChangeLoaderApp(true)

        axios('get', 'api/v1/todo').then(response => {
            this.setState({
                list: response.data
            })
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    toApplication = (application) => {
        this.props.navigation.navigate('ApplicationMyPage', {
            id: application.id,
            type: 0, //заявки в работе
        })
    }

    render() {
        return (
            <View style={styles.page}>
                <SafeAreaView style={{flex: 1}}>
                    <FlatList
                        data={this.state.list}
                        renderItem={({item}) => (
                            <TouchableOpacity
                                onPress={() => this.toApplication(item)}
                            >
                                <ApplacationCard
                                    data={item}
                                />
                            </TouchableOpacity>
                        )}
                        ListEmptyComponent={() => (
                            <NotDataBlock
                                title={'Заявок нет'}
                                text={'Возьмите парочку'}
                            />
                        )}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{padding: 20}}
                        onRefresh={() => this.loadList()}
                        refreshing={this.state.refreshing}
                    />
                </SafeAreaView>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Заявки в работе',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
})

export default ApplicationJob
