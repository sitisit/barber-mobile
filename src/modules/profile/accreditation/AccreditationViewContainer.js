// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import AccreditationView from './AccreditationView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(AccreditationView);
