import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Platform,
    Linking, TextInput
} from 'react-native';
import axios from "../../../plugins/axios";
import {LoaderApp} from "../../../components/LoaderApp";
import {DropDownHolder} from "../../../components/DropDownAlert";
import {Body, Button, CheckBox, ListItem, Textarea} from "native-base";
import components from "../../../styles/components";
import ModalHideBottom from "../../../components/ModalHideBottom";

class Accreditation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            entryList: [],
            accreditationList: [],

            openDialogMessage: false
        }
    }

    componentDidMount = () => {
        this.gitAccreditation()
        this.getAccreditationLoose()
    }

    gitAccreditation = () => {
        LoaderApp.isChangeLoaderApp(true)

        axios('get', 'api/v1/get-accreditation').then(response => {
            this.setState({
                list: response.data
            })
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    getAccreditationLoose = () => {
        axios('get', '/api/v1/get-accreditation?type=1').then(response => {
            this.setState({accreditationList: response.data})
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    getFinalListAccreditation = () => {
        let entryList = this.state.entryList
        let list = []

        entryList.map(id => {
            this.state.accreditationList.map(item => {
                if ( item.id == id ){
                    list.push(item.name)
                }
            })
        })

        return list.join(', ')
    }
    sendMessage = () => {
        if ( this.state.entryList.length <= 0 ){
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Выберите аккредитацию')
            return false
        }

        LoaderApp.isChangeLoaderApp(true)
        let data = {
            theme: 'Акредитации',
            message: this.state.message + ': ' + this.getFinalListAccreditation(),
        }

        axios('post', 'api/v1/send-message-to-admin', data).then(response => {
            this.setState({
                message: '',
                entryList: [],
                openDialogMessage: false
            })
            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Сообщение успешно отправлено')
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    entryRequest = (item) => {
        let entryList = this.state.entryList

        if (entryList.indexOf(item.id) == -1) {
            entryList.push(item.id)
        } else {
            entryList.splice(entryList.indexOf(item.id), 1)
        }

        this.setState({entryList})
    }

    render() {
        if (this.state.list.length <= 0) {
            return (
                <View style={[styles.page, {paddingVertical: 20}]}>
                    <View style={styles.bottomContent}>
                        <Text style={{fontSize: 16, marginBottom: 5, marginHorizontal: 20, textAlign: 'center'}}>Для
                            расширения списка аккредитаций</Text>
                        <Button full primary onPress={() => {
                            this.isClearTimetabe()
                        }}>
                            <Text style={{color: 'white'}}>ПОЗВОНИТЬ ОПЕРАТОРУ</Text>
                        </Button>
                    </View>
                </View>
            )
        }

        return (
            <View style={styles.page}>
                <ScrollView
                    style={{flex: 1}}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{padding: 20}}
                >
                    <Text style={{fontSize: 20, marginBottom: 10}}>Ваш список аккредитаций</Text>
                    {
                        this.state.list.map((item, idx) => (
                            <Text key={'acred-' + idx} style={styles.cardTitle}>{item.accreditation_id}</Text>
                        ))
                    }
                </ScrollView>

                <ModalHideBottom
                    open={this.state.openDialogMessage}
                    close={() => {
                        this.setState({openDialogMessage: false})
                    }}
                >
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{padding: 20}}
                    >
                        <Text style={{fontSize: 18, marginBottom: 10}}>Выберите интересующие вас аккредитации</Text>
                        <View style={{marginBottom: 20}}>
                            {
                                this.state.accreditationList.map((item, idx) => (
                                    <ListItem key={'item-' + idx} noIndent onPress={() => this.entryRequest(item)}>
                                        <CheckBox
                                            checked={this.state.entryList.indexOf(item.id) > -1}
                                            onPress={() => this.entryRequest(item)}
                                        />
                                        <Body style={{marginLeft: 5}}>
                                            <Text>{item.name}</Text>
                                        </Body>
                                    </ListItem>
                                ))
                            }
                        </View>

                        <Text style={{fontSize: 18, marginBottom: 10}}>Введите ваше сообщение</Text>
                        <View>
                            <Textarea
                                rowSpan={4}
                                value={this.state.message}
                                placeholder={'Сообщение...'}
                                onChangeText={(message) => {
                                    this.setState({message})
                                }}
                                style={[components.textarea, {
                                    marginBottom: 10,
                                    borderWidth: 1,
                                    borderStyle: 'solid',
                                    borderColor: 'black'
                                }]}
                            />
                            {
                                (this.state.showErrorCancel) ?
                                    <Text style={styles.cancelContentLabelError}>Необходимо заполнить причину
                                        отказа</Text> : <View/>
                            }
                        </View>
                        <Button full primary onPress={() => this.sendMessage()}>
                            <Text style={{color: 'white'}}>Написать</Text>
                        </Button>
                    </ScrollView>
                </ModalHideBottom>

                {
                    ( this.state.accreditationList.length > 0 ) && (
                        <View style={styles.bottomContent}>
                            <Text style={{fontSize: 16, marginBottom: 5, marginHorizontal: 20, textAlign: 'center'}}>
                                Для расширения списка аккредитаций напишите оператору
                            </Text>
                            <Button full primary onPress={() => {
                                this.setState({openDialogMessage: true})
                            }}>
                                <Text style={{color: 'white'}}>НАПИСАТЬ</Text>
                            </Button>
                        </View>
                    )
                }
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Аккредитация',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
    bottomContent: {
        paddingBottom: 5
    },

    card: {
        padding: 10,
        backgroundColor: 'white',
        marginBottom: 10,
        borderRadius: 3
    },
    cardTitle: {
        fontSize: 16,
    },
})

export default Accreditation
