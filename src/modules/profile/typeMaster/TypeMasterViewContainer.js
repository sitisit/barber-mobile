// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import TypeMasterView from './TypeMasterView';
import {setUser} from "../personalArea/PersonalAreaState";

export default compose(
  connect(
    state => ({
        user: state.user
    }),
    dispatch => ({
        setUser: (user) => dispatch(setUser(user)),
    }),
  ),
)(TypeMasterView);
