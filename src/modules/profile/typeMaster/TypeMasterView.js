import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import {CheckBox, ListItem, Body, Button, Radio} from 'native-base';
import {LoaderApp} from "../../../components/LoaderApp";
import {DropDownHolder} from "../../../components/DropDownAlert";
import color from "../../../styles/color";
import axios from "../../../plugins/axios";


class TypeMaster extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mobility: null,
            startMobility: null
        }
    }

    componentDidMount = () => {
        this.setState({
            mobility: this.props.user.user.mobility,
            startMobility: this.props.user.user.mobility,
        })
    }

    typesSave = () => {
        LoaderApp.isChangeLoaderApp(true)
        let data = {
            mobility: String(this.state.mobility)
        }

        axios('post', 'api/v1/set-mobility', data).then(response => {
            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Тип мастера успешно изменен.');
            this.setState({
                startMobility: this.state.mobility
            })
            this.loadUser()
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            LoaderApp.isChangeLoaderApp(false)
        })
    }
    changeType = (idx) => {
        this.setState({mobility: idx})
    }

    loadUser = () => {
        axios('get', 'api/v1/me').then(response => {
            this.props.setUser(response.data)
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            LoaderApp.isChangeLoaderApp(false)
        })
    }

    render() {
        return (
            <View style={styles.page}>
                <ScrollView
                    style={{flex: 1}}
                    showsVerticalScrollIndicator={false}
                >
                    <ListItem noIndent onPress={() => this.changeType(1)}>
                        <Radio color={"#da7b9e"} selected={this.state.mobility == 1} onPress={() => this.setState({mobility: "1"})}/>
                        <Body style={{marginLeft: 5}}>
                            <Text>стационарный</Text>
                        </Body>
                    </ListItem>
                    <ListItem noIndent onPress={() => this.changeType(2)}>
                        <Radio color={"#da7b9e"} selected={this.state.mobility == 2} onPress={() => this.setState({mobility: "2"})}/>
                        <Body style={{marginLeft: 5}}>
                            <Text>дежурный</Text>
                        </Body>
                    </ListItem>
                    <ListItem noIndent onPress={() => this.changeType(3)}>
                        <Radio color={"#da7b9e"} selected={this.state.mobility == 3} onPress={() => this.setState({mobility: "3"})}/>
                        <Body style={{marginLeft: 5}}>
                            <Text>разовый</Text>
                        </Body>
                    </ListItem>
                </ScrollView>

                <View style={[styles.bottomContent, (this.state.startMobility != this.state.mobility)? {bottom: 0}: '']}>
                    <Button full primary onPress={() => {this.typesSave()}}>
                        <Text style={{color: 'white'}}>Сохранить</Text>
                    </Button>
                </View>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Тип мастера',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },

    bottomContent: {
        position: 'relative',
        bottom: -100,
        paddingBottom: 10
    }
})

export default TypeMaster
