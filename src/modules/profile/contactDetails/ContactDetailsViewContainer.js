// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ContactDetailsView from './ContactDetailsView';

export default compose(
  connect(
    state => ({
        user: state.user
    }),
    dispatch => ({}),
  ),
)(ContactDetailsView);
