import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    KeyboardAvoidingView,
    TextInput
} from 'react-native';
import TextInputView from "../../../components/TextInputView";
import color from "../../../styles/color";
import {Button, Textarea} from "native-base";
import {LoaderApp} from "../../../components/LoaderApp";
import {DropDownHolder} from "../../../components/DropDownAlert";
import components from "../../../styles/components";
import {TextInputMask} from "react-native-masked-text";
import ModalHideBottom from "../../../components/ModalHideBottom";
import axios from "../../../plugins/axios";


class ContactDetails extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            lastname: '',
            tel: '',
            whatsapp: '',
            email: '',
            area: [],

            openDialogMessage: false,
            message: '',
        }
    }

    componentDidMount = () => {
        let user = this.props.user.user

        console.log('user:', user)

        this.setState({
            name: user.name,
            lastname: user.lastname,
            tel: user.tel,
            whatsapp: user.whatsapp,
            email: user.email
        })
    }

    saveFormData = () => {
        LoaderApp.isChangeLoaderApp(true)
        setTimeout(() => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Ваши контактные данные изменены.');
        }, 3000)
    }

    sendMessage = () => {
        LoaderApp.isChangeLoaderApp(true)
        let data = {
            theme: 'Контакты',
            message: this.state.message
        }

        axios('post', 'api/v1/send-message-to-admin', data).then(response => {
            this.setState({
                message: '',
                openDialogMessage: false
            })
            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Сообщение успешно отправлено')
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            LoaderApp.isChangeLoaderApp(false)
        })
    }

    render() {
        return (
            <View style={styles.page}>
                <KeyboardAvoidingView
                    behavior="position"
                    enabled
                    style={styles.page}
                >
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{padding: 20}}
                    >
                        <View style={styles.row}>
                            <Text style={styles.rowText}>Имя: <Text style={styles.rowTextValue}>{this.state.name}</Text></Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.rowText}>Фамилия: <Text style={styles.rowTextValue}>{this.state.lastname}</Text></Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.rowText}>Контактный телефон: <Text style={styles.rowTextValue}>{this.state.tel}</Text></Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.rowText}>Whatsapp: <Text style={styles.rowTextValue}>{this.state.whatsapp}</Text></Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.rowText}>E-mail: <Text style={styles.rowTextValue}>{this.state.email}</Text></Text>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>

                <View style={styles.bottomContent}>
                    <Text style={{fontSize: 16, marginBottom: 5, marginHorizontal: 20, textAlign: 'center'}}>
                        Для изменения контактной информация напишите оператору
                    </Text>
                    <Button full primary onPress={() => {this.setState({openDialogMessage: true})}}>
                        <Text style={{color: 'white'}}>Написать</Text>
                    </Button>
                </View>

                <ModalHideBottom
                    open={this.state.openDialogMessage}
                    close={() => {
                        this.setState({openDialogMessage: false})
                    }}
                >
                    <View style={{padding: 20}}>
                        <Text style={{fontSize: 18, marginBottom: 10}}>Введите ваше сообщение</Text>
                        <View>
                            <Textarea
                                rowSpan={4}
                                value={this.state.message}
                                placeholder={'Сообщение...'}
                                onChangeText={(message) => {
                                    this.setState({message})
                                }}
                                style={[components.textarea, {marginBottom: 10, borderWidth: 1, borderStyle: 'solid', borderColor: 'black'}]}
                            />
                            {
                                (this.state.showErrorCancel) ?
                                    <Text style={styles.cancelContentLabelError}>Необходимо заполнить причину
                                        отказа</Text> : <View/>
                            }
                        </View>
                        <Button full primary onPress={() => this.sendMessage()}>
                            <Text style={{color: 'white'}}>НАПИСАТЬ</Text>
                        </Button>
                    </View>
                </ModalHideBottom>
            </View>
        );
    }44

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Контактные данные',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: color.page
    },

    textInputView: {
        marginBottom: 20
    },

    bottomContent: {
        paddingBottom: 5
    },

    row: {
        flexDirection: 'row'
    },
    rowText: {
        fontSize: 18,
    },
    rowTextValue: {
        fontWeight: 'bold'
    },


    input: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: 'white',
        borderRadius: 2,
        justifyContent: 'center'
    },
})

export default ContactDetails
