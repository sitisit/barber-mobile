// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import NotificationsView from './NotificationsView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(NotificationsView);
