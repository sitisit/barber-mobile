import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import NotDataBlock from "../../../components/NotDataBlock";
import axios from "../../../plugins/axios";
import {LoaderApp} from "../../../components/LoaderApp";
import {DropDownHolder} from "../../../components/DropDownAlert";

class Notifications extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: []
        }
    }

    componentDidMount = () => {
        this.getListNotification()
    }

    getListNotification = () => {
        LoaderApp.isChangeLoaderApp(true)

        axios('get', '/api/v1/get-notifications').then(response => {
            LoaderApp.isChangeLoaderApp(false)
            this.setState({list: response.data})
            console.log('response: ', response)
        }).catch(error => {
            console.log('error: ', error.response)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            LoaderApp.isChangeLoaderApp(false)
        })
    }

    render() {
        if ( this.state.list.length == 0 ){
            return (
                <View style={styles.page}>
                    <NotDataBlock
                        title={'Нет уведомлений'}
                    />
                </View>
            )
        }
        return (
            <View style={styles.page}>
                <ScrollView
                    style={{flex: 1}}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{padding: 20, paddingBottom: 10}}
                >
                    {
                        this.state.list.map((item, idx) => (
                            <View key={idx} style={styles.card}>
                                <Text style={styles.cardTitle}>{item.title}</Text>
                                <Text style={styles.cardDescription}>{item.text}</Text>
                            </View>
                        ))
                    }
                </ScrollView>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Уведомления',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },

    card: {
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 3,
        marginBottom: 10,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.2,
        shadowRadius: 0,
        elevation: 3,
    },
    cardTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 5
    },
    cardDescription: {
        fontSize: 16,
        color: '#4F4F4F'
    }
})

export default Notifications
