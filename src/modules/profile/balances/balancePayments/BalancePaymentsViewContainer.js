// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import BalancePaymentsView from './BalancePaymentsView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(BalancePaymentsView);
