import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity, TextInput,
} from 'react-native';
import color from "../../../../styles/color";
import axios from "../../../../plugins/axios";
import {DropDownHolder} from "../../../../components/DropDownAlert";
import {LoaderApp} from "../../../../components/LoaderApp";
import {getPrice} from "../../../../helper/formatting";
import components from "../../../../styles/components";
import {Button, Textarea} from "native-base";


class BalancePayments extends Component {
    constructor(props) {
        super(props);

        this.state = {
            balance: 0,

            message: ''
        }
    }

    componentDidMount = () => {
        this.getBalance()
    }

    getBalance = () => {
        axios('get', 'api/v1/get-balance').then(response => {
            this.setState({
                balance: response.data
            })
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    sendMessage = () => {
        LoaderApp.isChangeLoaderApp(true)
        let data = {
            theme: 'Баланс пополнение',
            message: this.state.message,
        }

        axios('post', 'api/v1/send-message-to-admin', data).then(response => {
            this.setState({
                message: '',
            })
            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Сообщение успешно отправлено')
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    render() {
        return (
            <View style={styles.page}>
                <ScrollView
                    style={{flex: 1}}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{padding: 20}}
                >
                    <View style={styles.balanceContainer}>
                        <Text style={styles.balance}>
                            <Text style={{fontWeight: 'bold'}}>На вашем счету: </Text>
                            {getPrice(this.state.balance)}
                        </Text>
                    </View>

                    <View style={styles.balanceContainer}>
                        <Text style={{fontSize: 14, marginBottom: 5}}>Для пополнения денежных средств, напишите оператору.</Text>
                        <Textarea
                            rowSpan={4}
                            value={this.state.message}
                            placeholder={'Сообщение...'}
                            onChangeText={(message) => {
                                this.setState({message})
                            }}
                            style={[components.textarea, {marginBottom: 10, borderWidth: 1, borderStyle: 'solid', borderColor: 'black'}]}
                        />
                        <Button full primary onPress={() => this.sendMessage()}>
                            <Text style={{color: 'white'}}>Написать</Text>
                        </Button>
                    </View>

                </ScrollView>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Пополнение счета',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: color.page
    },

    balanceContainer: {
        marginBottom: 10,
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 2
    },
    balance: {
        fontSize: 20
    },
})

export default BalancePayments
