// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import BalanceView from './BalanceView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(BalanceView);
