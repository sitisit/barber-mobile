import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import {Button, Icon, List, ListItem} from "native-base";
import color from "../../../../styles/color";
import {getPrice, getCountOrder} from '../../../../helper/formatting'
import axios from "../../../../plugins/axios";
import {LoaderApp} from "../../../../components/LoaderApp";
import {DropDownHolder} from "../../../../components/DropDownAlert";

const operationCodes = {
    1: 'Пополнение',
    2: 'Расход',
    3: 'Заблокировано',
    4: 'Вывод средств',
}


class Balance extends Component {
    constructor(props) {
        super(props);

        this.state = {
            balance: 0,
            hold_balance: 0,
            available_balance: 0,
            history: [],
        }
    }

    componentDidMount = () => {
        LoaderApp.isChangeLoaderApp(true)

        this.getBalance()
        this.getHoldBalance()
        this.getAvailableBalance()
        this.getBalanceHistory()
    }

    getBalance = () => {
        axios('get', 'api/v1/get-balance').then(response => {
            this.setState({
                balance: response.data
            })
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    getAvailableBalance = () => {
        axios('get', 'api/v1/get-available-balance').then(response => {
            this.setState({
                available_balance: response.data
            })
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    getHoldBalance = () => {
        axios('get', 'api/v1/get-hold-balance').then(response => {
            this.setState({
                hold_balance: response.data
            })
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    getBalanceHistory = () => {
        axios('get', 'api/v1/balance-history').then(response => {
            this.setState({
                history: response.data
            })

            console.log(response.data)
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            LoaderApp.isChangeLoaderApp(false)
        })
    }

    render() {
        return (
            <View style={styles.page}>
                <View
                    style={{flex: 1, padding: 20}}
                >
                    <View style={styles.balanceContainer}>
                        <Text style={styles.balance}>
                            <Text style={{fontWeight: 'bold'}}>На вашем счету: </Text>
                            {getPrice(this.state.balance)}
                        </Text>
                        <Text style={styles.balance}>
                            <Text style={{fontWeight: 'bold'}}>Доступно: </Text>
                            {getPrice(this.state.available_balance)}
                        </Text>
                        <Text style={styles.balance}>
                            <Text style={{fontWeight: 'bold'}}>Заблокировано: </Text>
                            {getPrice(this.state.hold_balance)}
                        </Text>
                    </View>

                    {
                        (this.state.history.length > 0) && (
                            <View style={styles.historyContent}>
                                <ScrollView
                                    style={{flex: 1}}
                                    showsVerticalScrollIndicator={false}
                                    contentContainerStyle={{paddingHorizontal: 20, paddingVertical: 10}}
                                >
                                    {
                                        this.state.history.map((item, idx) => (
                                            <View key={'balance-item-' + idx} style={styles.cardHistory}>
                                                <Text style={styles.cardHistoryType}>{operationCodes[item.operation]}</Text>
                                                <Text style={styles.cardHistorySumma}>Сумма: {getPrice(item.value)}</Text>
                                            </View>
                                        ))
                                    }
                                </ScrollView>
                            </View>
                        )
                    }
                </View>
                <View style={styles.bottomContent}>
                    <Button full primary style={{marginBottom: 5}} onPress={() => { this.props.navigation.navigate('BalancePayments')} }>
                        <Text style={{color: 'white'}}>Пополнить</Text>
                    </Button>
                    <Button full primary onPress={() => { this.props.navigation.navigate('BalanceConclusion')} }>
                        <Text style={{color: 'white'}}>Вывод средств</Text>
                    </Button>
                </View>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Баланс',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: color.page
    },

    headerRight: {
        padding: 20
    },

    balanceContainer: {
        marginBottom: 10,
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 2
    },
    balance: {
        fontSize: 20
    },

    historyContent: {
        flex: 1,
        backgroundColor: 'white',
        borderRadius: 2
    },
    cardHistory: {
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: 'black',
        paddingVertical: 10
    },
    cardHistoryType: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    cardHistoryOrder: {
        fontSize: 18,
    },
    cardHistorySumma: {
        fontSize: 18,
    },

    bottomContent: {
        paddingBottom: 5
    }
})

export default Balance
