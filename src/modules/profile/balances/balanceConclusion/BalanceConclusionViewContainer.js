// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import BalanceConclusionView from './BalanceConclusionView';

export default compose(
  connect(
    state => ({
        user: state.user
    }),
    dispatch => ({}),
  ),
)(BalanceConclusionView);
