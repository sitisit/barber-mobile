// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ChangePhoneNumberView from './ChangePhoneNumberView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(ChangePhoneNumberView);
