// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import SettingFilterApplicationView from './SettingFilterApplicationView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(SettingFilterApplicationView);
