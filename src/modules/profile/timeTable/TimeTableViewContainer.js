// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import TimeTableView from './TimeTableView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(TimeTableView);
