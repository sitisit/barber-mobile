import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    TouchableWithoutFeedback,
} from 'react-native';
import color from "../../../styles/color";
import {Icon} from "native-base";
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import RBSheet from "react-native-raw-bottom-sheet";
import {getDateFromBackend} from "../../../helper/formatting"
import axios from "../../../plugins/axios";
import {DropDownHolder} from "../../../components/DropDownAlert";
import {LoaderApp} from "../../../components/LoaderApp";

class CalendarTimeTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dates: {},
            information: {},
            shedule: {},

            dayTimeStart: null,
            dayTimeEnd: null,

            showCalendarList: false,
            openInfo: false,
        }

        this.ModalHideBottom = React.createRef();
        this.CalendarList = React.createRef();
    }

    componentDidMount = () => {
        this.isGetTimeJob()
    }
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        console.log('prevProps: ', prevProps)
        console.log('props: ', this.props)
        if (Object.keys(prevProps.date).length != Object.keys(this.props.date).length) {
            this.setState({showCalendarList: false})

            setTimeout(() => {
                this.setState({showCalendarList: true})
            }, 1)
        }
    }

    isGetTimeJob = () => {
        LoaderApp.isChangeLoaderApp(true)
        axios('get', 'api/v1/get-time-range').then(response => {
            this.setState({
                dayTimeStart: response.data[0],
                dayTimeEnd: response.data[response.data.length - 1],
                showCalendarList: true
            })
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    createDay = (date) => {
        let shedule = {
            date: date.year + '-' + (('0' + String(date.month)).slice(-2)) + '-' + (('0' + String(date.day)).slice(-2)),
            time_start: this.state.dayTimeStart,
            time_end: this.state.dayTimeEnd,
        }

        this.setState({
            shedule,
            openInfo: true
        })

        this.ModalHideBottom.open()
    }

    getStatusDay = (date) => {
        let timestamp = date.dateString;
        let shedules = this.props.date[timestamp]

        if (shedules) {
            return shedules
        }

        return null
    }
    openInformation = (shedule) => {
        this.setState({
            shedule,
            openInfo: true
        })

        this.ModalHideBottom.open()
    }

    render() {
        if (!this.state.showCalendarList) {
            return (
                <View>
                    <CalendarList
                        style={[styles.calendar]}
                        firstDay={1}
                        pastScrollRange={0}
                        futureScrollRange={1}
                        dayComponent={({date}) => {
                            return (
                                <View style={{padding: 5}}>
                                    <Text style={{textAlign: 'center'}}>
                                        {date.day}
                                    </Text>
                                </View>
                            );
                        }}
                    />
                </View>
            )
        }
        return (
            <View>
                {
                    (this.state.showCalendarList) && (
                        <CalendarList
                            ref={this.CalendarList}
                            style={[styles.calendar]}
                            firstDay={1}
                            pastScrollRange={0}
                            futureScrollRange={1}
                            timezone={'America/Los_Angelas'}
                            dayComponent={({date}) => {
                                let shedule = this.getStatusDay(date)

                                if (shedule) {
                                    return (
                                        <TouchableOpacity
                                            style={{padding: 5}}
                                            onPress={() => this.openInformation(shedule)}>
                                            <Text
                                                style={{textAlign: 'center', fontWeight: 'bold', color: color.primary}}>
                                                {date.day}
                                            </Text>
                                        </TouchableOpacity
                                        >
                                    );
                                }

                                return (
                                    <TouchableOpacity
                                        style={{padding: 5}}
                                        onPress={() => this.createDay(date)}
                                    >
                                        <View style={{padding: 5}}>
                                            <Text style={{textAlign: 'center'}}>
                                                {date.day}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                );
                            }}
                        />
                    )
                }

                <RBSheet
                    ref={ref => {
                        this.ModalHideBottom = ref
                    }}
                    onClose={() => this.setState({openInfo: false})}
                    closeOnDragDown
                >
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{flex: 1}}
                    >
                        <TouchableWithoutFeedback>
                            <View style={styles.infoShedule}>
                                <View style={styles.infoSheduleRow}>
                                    <Text style={styles.infoSheduleTitle}>Дата:&nbsp;</Text>
                                    <Text
                                        style={styles.infoSheduleText}>{getDateFromBackend(this.state.shedule.date)}</Text>
                                </View>
                                <View style={styles.infoSheduleRow}>
                                    <Text style={styles.infoSheduleTitle}>Начало рабочего дня:&nbsp;</Text>
                                    <Text style={styles.infoSheduleText}>{this.state.shedule.time_start}:00</Text>
                                </View>
                                <View style={styles.infoSheduleRow}>
                                    <Text style={styles.infoSheduleTitle}>Конец рабочего дня:&nbsp;</Text>
                                    <Text style={styles.infoSheduleText}>{this.state.shedule.time_end}:00</Text>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </ScrollView>
                </RBSheet>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    calendar: {
        minHeight: 300,
    },

    infoShedule: {
        padding: 10
    },
    infoSheduleRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    infoSheduleTitle: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    infoSheduleText: {
        fontSize: 18,
    }
})

export default CalendarTimeTable
