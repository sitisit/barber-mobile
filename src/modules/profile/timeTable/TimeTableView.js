import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    SafeAreaView, FlatList,
    TouchableWithoutFeedback,
    Dimensions
} from 'react-native';
import color from "../../../styles/color";
import {Body, Button, CheckBox, Icon, ListItem, Radio, Switch, Left, Right} from "native-base";
import axios from "../../../plugins/axios";
import {LoaderApp} from "../../../components/LoaderApp";
import {DropDownHolder} from "../../../components/DropDownAlert";
import CalendarTimeTable from './Calendar';
import {CalendarList} from "react-native-calendars";
import {getDateFromBackend, timestampToDate, getDateToBackend} from "../../../helper/formatting";
import RBSheet from "react-native-raw-bottom-sheet";

const heightScreen = Dimensions.get('window').height;

class TimeTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            refreshing: false,

            list: {},

            showButtonClearAll: false,

            information: {},
            shedule: {
                list: []
            },

            dayTimeList: [],
            dayTimeStart: null,
            dayTimeEnd: null,

            openInfo: false,

            updateCalendar: true
        }

        this.ModalHideBottom = React.createRef();
        this.CalendarList = React.createRef();
    }

    componentDidMount = () => {
        this.isGetTimeJob()
        LoaderApp.isChangeLoaderApp(true)

        this.props.navigation.addListener('didFocus', () => {
            LoaderApp.isChangeLoaderApp(true)
            setTimeout(() => {
                this.getTimeTable()
            }, 500)
        });
    }

    getTimeTable = () => {
        this.setState({updateCalendar: true})
        LoaderApp.isChangeLoaderApp(true)
        axios('get', 'api/v1/get-timetable-dates').then(response => {
            let object = {}

            response.data.map(item => {
                object[item] = {}
            })

            this.setState({
                list: object,
                updateCalendar: false
            })

            if (Object.keys(object).length > 0) {
                this.setState({
                    showButtonClearAll: true
                })
            } else {
                this.setState({
                    showButtonClearAll: false
                })
            }
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            LoaderApp.isChangeLoaderApp(false)
        })
    }
    isClearTimetabe = () => {
        LoaderApp.isChangeLoaderApp(true)
        axios('delete', 'api/v1/del-timetable').then(response => {
            this.getTimeTable()
            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Расписание полностью удалено')
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            LoaderApp.isChangeLoaderApp(false)
        })
    }

    isGetTimeJob = () => {
        LoaderApp.isChangeLoaderApp(true)
        axios('get', 'api/v1/get-time-range').then(response => {
            this.setState({
                dayTimeStart: response.data[0],
                dayTimeEnd: response.data[response.data.length - 1],
                dayTimeList: response.data
            })
            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    getSheduleDay = (date) => {
        date = getDateFromBackend(date.dateString)

        LoaderApp.isChangeLoaderApp(true)
        axios('get', 'api/v1/get-timetable?date=' + date).then(response => {
            let list = response.data

            if (list.length > 0) {
                this.setReadySchedule(list, date)
            } else {
                this.newReadySchedule(date)
            }

            LoaderApp.isChangeLoaderApp(false)
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    setReadySchedule = (list, date) => {
        let index = 0

        list.map(item => {
            if (item.active == 1) {
                index++
            }
        })

        let shedule = {
            date,
            time_start: this.state.dayTimeStart,
            time_end: this.state.dayTimeEnd,
            list: list,
            allDay: list.length == index
        }

        this.setState({
            shedule,
            openInfo: true
        })

        this.ModalHideBottom.open()
    }
    newReadySchedule = (date) => {
        let list = []

        this.state.dayTimeList.map((item, idx) => {
            if (this.state.dayTimeList.length - 1 > idx) {
                list.push({
                    time_start: item,
                    time_end: item + 1,
                    active: '0'
                })
            }
        })

        let shedule = {
            date,
            time_start: this.state.dayTimeStart,
            time_end: this.state.dayTimeEnd,
            list: list,
            allDay: false
        }

        this.setState({
            shedule,
            openInfo: true
        })

        this.ModalHideBottom.open()
    }

    getStatusDay = (date) => {
        let timestamp = date.dateString;
        let shedules = this.state.list[timestamp]

        if (shedules) {
            shedules['time_list'] = [...this.state.dayTimeList]
            return shedules
        }

        return null
    }
    saveEditDay = () => {
        let shedule = this.state.shedule
        let date = getDateToBackend(shedule.date)

        let data = []

        shedule.list.map(item => {
            data.push({
                start_date: date,
                time_start: item.time_start,
                time_end: item.time_end,
                active: (shedule.allDay)? shedule.allDay: item.active,
                type: 2
            })
        })

        LoaderApp.isChangeLoaderApp(true)

        axios('post', 'api/v1/set-timetable', data).then(response => {
            LoaderApp.isChangeLoaderApp(false)
            this.getTimeTable()
            this.setState({shedule: {list: []}})
        }).catch(error => {
            LoaderApp.isChangeLoaderApp(false)
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    sheduleEntryAllDay = () => {
        let shedule = this.state.shedule

        shedule.list.map(item => {
            item.active = 1
        })

        this.setState({
            shedule
        })
    }

    render() {
        if (this.state.updateCalendar) {
            return (
                <View>
                    <CalendarList
                        style={[styles.calendar]}
                        firstDay={1}
                        pastScrollRange={0}
                        futureScrollRange={1}
                        dayComponent={({date}) => {
                            return (
                                <View style={{padding: 5}}>
                                    <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                                        {date.day}
                                    </Text>
                                </View>
                            );
                        }}
                    />
                </View>
            )
        }

        return (
            <View style={styles.page}>
                <View style={styles.topContent}>
                    <TouchableOpacity style={styles.buttonUpdate} onPress={() => this.getTimeTable()}>
                        <Icon
                            name="redo"
                            type="FontAwesome5"
                            style={{color: 'white', fontSize: 20}}
                        />
                    </TouchableOpacity>
                </View>
                <View style={{flex: 1}}>
                    <CalendarList
                        ref={this.CalendarList}
                        style={[styles.calendar]}
                        firstDay={1}
                        pastScrollRange={0}
                        futureScrollRange={1}
                        timezone={'America/Los_Angelas'}
                        dayComponent={({date}) => {
                            let shedule = this.getStatusDay(date)

                            if (shedule) {
                                return (
                                    <TouchableOpacity
                                        style={{padding: 5}}
                                        onPress={() => this.getSheduleDay(date)}
                                    >
                                        <Text
                                            style={{textAlign: 'center', fontWeight: 'bold', color: color.primary}}>
                                            {date.day}
                                        </Text>
                                    </TouchableOpacity
                                    >
                                );
                            }

                            return (
                                <TouchableOpacity
                                    style={{padding: 5}}
                                    onPress={() => this.getSheduleDay(date)}
                                >
                                    <View>
                                        <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                                            {date.day}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        }}
                    />
                </View>
                {
                    (this.state.showButtonClearAll) && (
                        <View style={styles.bottomContent}>
                            <Button full primary onPress={() => {
                                this.isClearTimetabe()
                            }} style={{backgroundColor: color.error}}>
                                <Text style={{color: 'white'}}>УДАЛИТЬ ВСЕ</Text>
                            </Button>
                        </View>
                    )
                }
                <RBSheet
                    ref={ref => {
                        this.ModalHideBottom = ref
                    }}
                    height={heightScreen - 50}
                    onClose={() => this.setState({openInfo: false})}
                    closeOnDragDown
                >
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <TouchableWithoutFeedback>
                            <View style={styles.infoShedule}>
                                <ScrollView
                                    showsVerticalScrollIndicator={false}
                                >
                                    <View style={styles.infoSheduleRow}>
                                        <Text style={styles.infoSheduleTitle}>Дата:&nbsp;</Text>
                                        <Text
                                            style={styles.infoSheduleText}>{getDateFromBackend(this.state.shedule.date)}</Text>
                                    </View>
                                    <View style={styles.infoSheduleRow}>
                                        <Text style={styles.infoSheduleTitle}>Начало рабочего дня:&nbsp;</Text>
                                        <Text style={styles.infoSheduleText}>{this.state.shedule.time_start}:00</Text>
                                    </View>
                                    <View style={styles.infoSheduleRow}>
                                        <Text style={styles.infoSheduleTitle}>Конец рабочего дня:&nbsp;</Text>
                                        <Text style={styles.infoSheduleText}>{this.state.shedule.time_end}:00</Text>
                                    </View>

                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        marginVertical: 10
                                    }}>
                                        <Text style={{fontSize: 18, fontWeight: 'bold'}}>Весь день</Text>
                                        <Switch
                                            value={this.state.shedule.allDay}

                                            trackColor={{ false: "#d8d8d8", true: "#d8d8d8" }}
                                            thumbColor={this.state.shedule.allDay ? "#da7b9e" : "#f4f3f4"}
                                            ios_backgroundColor="#da7b9e"

                                            onValueChange={() => {
                                                this.state.shedule.allDay = !this.state.shedule.allDay;

                                                if ( this.state.shedule.allDay ){
                                                    this.state.shedule.list.map(item => {
                                                        item.active = true
                                                    })
                                                }

                                                this.setState({shedule: this.state.shedule})
                                            }}
                                        />
                                    </View>

                                    {
                                        (this.state.shedule.list.length > 0) && (
                                            <View style={styles.contentShedules}>
                                                <Text style={{flex: 1, marginBottom: 5}}>Выберите промежутки когда вы
                                                    хотите работать
                                                </Text>

                                                <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                                                    {
                                                        this.state.shedule.list.map((item, idx) => (
                                                            <TouchableWithoutFeedback onPress={() => {
                                                                let index = 0
                                                                if (item.active == 1) {
                                                                    item.active = 0
                                                                } else {
                                                                    item.active = 1
                                                                }
                                                                this.state.shedule.list.map(item => {
                                                                    if (item.active == 1) {
                                                                        index++
                                                                    }
                                                                })
                                                                this.state.shedule.allDay = this.state.shedule.list.length == index
                                                                this.setState({shedule: this.state.shedule})
                                                            }}>
                                                                <View style={{flexDirection: 'row', width: '50%', marginVertical: 5}}>
                                                                    <CheckBox checked={item.active == 1} onPress={() => {
                                                                        let index = 0
                                                                        if (item.active == 1) {
                                                                            item.active = 0
                                                                        } else {
                                                                            item.active = 1
                                                                        }
                                                                        this.state.shedule.list.map(item => {
                                                                            if (item.active == 1) {
                                                                                index++
                                                                            }
                                                                        })
                                                                        this.state.shedule.allDay = this.state.shedule.list.length == index
                                                                        this.setState({shedule: this.state.shedule})
                                                                    }}/>
                                                                    <Body style={{marginLeft: 5}}>
                                                                        <Text>{('0' + item.time_start).slice(-2)}:00
                                                                            - {('0' + item.time_end).slice(-2)}:00</Text>
                                                                    </Body>
                                                                </View>
                                                            </TouchableWithoutFeedback>
                                                        ))
                                                    }
                                                </View>
                                            </View>
                                        )
                                    }

                                    <Button
                                        full
                                        primary
                                        onPress={() => {
                                            this.saveEditDay()
                                        }}
                                        style={{marginTop: 10}}
                                    >
                                        <Text style={{color: 'white'}}>Сохранить</Text>
                                    </Button>
                                </ScrollView>
                            </View>
                        </TouchableWithoutFeedback>
                    </ScrollView>
                </RBSheet>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Расписание',
            headerRight: () => (
                <TouchableOpacity
                    style={styles.headerRight}
                    onPress={() => navigation.navigate('SettingsTimeTable')}
                >
                    <Icon
                        name="cog"
                        type="FontAwesome"
                        style={{color: 'white'}}
                    />
                </TouchableOpacity>

            )
        };
    };
}

const styles = StyleSheet.create({

    contentShedules: {
        marginTop: 10
    },
    contentShedulesItem: {
        marginBottom: 5,
    },

    page: {
        flex: 1,
        backgroundColor: color.page
    },
    headerRight: {
        padding: 10
    },

    topContent: {
        position: 'absolute',
        zIndex: 1,
        top: 10,
        right: 20,
    },

    buttonUpdate: {
        width: 35,
        height: 35,
        borderRadius: 3,
        backgroundColor: color.primary,

        justifyContent: 'center',
        alignItems: 'center'
    },

    textNotShedules: {
        fontSize: 22,
        textAlign: 'center',
        fontWeight: 'bold',
        marginBottom: 10
    },


    bottomContent: {
        paddingBottom: 5
    },

    calendar: {
        minHeight: 300,
    },

    infoShedule: {
        padding: 10
    },
    infoSheduleRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    infoSheduleTitle: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    infoSheduleText: {
        fontSize: 18,
    }
})

export default TimeTable
