import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    Picker,
    TouchableWithoutFeedback,
    Platform
} from 'react-native';
import components from "../../../../styles/components";
import color from "../../../../styles/color";
import {TextInputMask} from "react-native-masked-text";
import DateTimePicker from '@react-native-community/datetimepicker';
import {timestampToDate, timestampToTime} from "../../../../helper/formatting";
import {Button} from "native-base";
import axios from "../../../../plugins/axios";
import {LoaderApp} from "../../../../components/LoaderApp";
import {Select} from "../../../../components/Select";
import {DropDownHolder} from "../../../../components/DropDownAlert";

class TimeTableSettings extends Component {
    constructor(props) {
        super(props);

        this.state = {
            date_start: new Date(),
            date_work: '5',
            date_weekend: '2',

            day_time_start: '',
            day_time_end: '',

            count_weeks: 1,

            showDateStart: false,

            timeJob: [],
            listWeeks: [
                {
                    label: '1',
                    value: 1
                },
                {
                    label: '2',
                    value: 2
                },
                {
                    label: '3',
                    value: 3
                },
                {
                    label: '4',
                    value: 4
                },
                {
                    label: '5',
                    value: 5
                },
            ]

        }
    }

    componentDidMount = () => {
        this.isGetTimeJob()
    }

    isErrorWorkDay = () => {
        if ((Number(this.state.date_work) + Number(this.state.date_weekend)) > 7) {
            return true
        }
    }
    isGetTimeJob = () => {
        axios('get', 'api/v1/get-time-range').then(response => {
            this.setState({
                timeJob: response.data,
                day_time_start: response.data[0],
                day_time_end: response.data[response.data.length - 1],
            })
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }

    onHideDate = () => {
        if (this.state.showDateStart) {
            this.setState({
                showDateStart: false
            })
        }
    }

    save = () => {
        if (this.validation()) {
            LoaderApp.isChangeLoaderApp(true)

            let data = [{
                start_date: timestampToDate(this.state.date_start),
                weeks: this.state.count_weeks,
                work_days: this.state.date_work,
                hollydays: this.state.date_weekend,
                time_start: this.state.day_time_start,
                time_end: this.state.day_time_end,
                active: true,
                type: 1
            }]

            console.log('data: ', data)

            axios('post', 'api/v1/set-timetable', data).then(response => {
                LoaderApp.isChangeLoaderApp(false)
                this.props.navigation.goBack()
            }).catch(error => {
                LoaderApp.isChangeLoaderApp(false)
                DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            })
        }
    }
    validation = () => {
        let bool = true

        if (!this.state.date_work || this.state.date_work == 0) {
            bool = false
        }
        if (!this.state.date_weekend) {
            bool = false
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Заполните выходные дни')
        }
        if (!this.state.count_weeks) {
            bool = false
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Заполните количество недель')
        }
        if (!this.state.day_time_start || !this.state.day_time_end) {
            bool = false
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Заполните рабочие часы')
        }

        return bool
    }

    isGetStartTime = () => {
        let list = this.state.timeJob.slice()
        let array = []

        if (list && list.length > 0) {
            list.pop()

            list.map(item => {
                array.push({
                    label: String(item),
                    value: item
                })
            })
        }

        return array
    }
    isGetEndTime = () => {
        let list = this.state.timeJob.slice()
        let array = []

        if (list && list.length > 0) {
            let idxDayTimeStart = this.state.timeJob.indexOf(this.state.day_time_start)

            list.splice(0, idxDayTimeStart + 1)

            if (this.state.day_time_start >= this.state.day_time_end || !this.state.day_time_end) {
                this.setState({
                    day_time_end: list[0]
                })
            }

            list.map(item => {
                array.push({
                    label: String(item),
                    value: item
                })
            })
        }


        return array
    }

    render() {
        return (
            <View style={styles.page}>
                <KeyboardAvoidingView
                    behavior={Platform.Os == "ios" ? "padding" : "height"}
                    style={styles.page}
                >
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{padding: 20}}
                    >
                        <View style={styles.bodyItem}>
                            <Text style={styles.bodyItemTitle}>График работы: </Text>
                            <View style={[styles.row, {flexDirection: 'column'}]}>
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <TextInput
                                        value={this.state.date_work}
                                        type={'number'}
                                        style={[components.input, styles.input, (this.isErrorWorkDay()) ? {backgroundColor: 'rgba(255, 0, 0, 0.20)'} : '']}
                                        placeholder="Рабочие дни"
                                        onChangeText={(date_work) => {
                                            this.setState({date_work})
                                        }}
                                        placeholderTextColor={color.placeholder}
                                        keyboardType="number-pad"
                                        maxLength={1}
                                    />
                                    <Text style={{paddingHorizontal: 10}}>/</Text>
                                    <TextInput
                                        value={this.state.date_weekend}
                                        type={'number'}
                                        style={[components.input, styles.input, (this.isErrorWorkDay()) ? {backgroundColor: 'rgba(255, 0, 0, 0.20)'} : '']}
                                        placeholder="Выходные дни"
                                        onChangeText={(date_weekend) => {
                                            this.setState({date_weekend})
                                        }}
                                        placeholderTextColor={color.placeholder}
                                        keyboardType="number-pad"
                                        maxLength={1}
                                    />
                                </View>
                                {
                                    (this.isErrorWorkDay()) && (
                                        <Text style={styles.error}>
                                            Общие колличество дней не может быть больше 7
                                        </Text>
                                    )
                                }
                                {
                                    (!this.state.date_work || this.state.date_work == 0) && (
                                        <Text style={styles.error}>
                                            Минимум 1
                                        </Text>
                                    )
                                }
                            </View>
                        </View>
                        <View style={styles.bodyItem}>
                            <Text style={styles.bodyItemTitle}>Начать график: </Text>
                            <TouchableOpacity
                                onPress={() => this.setState({showDateStart: true})}
                                style={[styles.row, components.input]}
                            >
                                <Text>{timestampToDate(this.state.date_start)}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.bodyItem}>
                            <Text style={styles.bodyItemTitle}>Рабочие часы: </Text>
                            <View style={styles.row}>
                                <Select
                                    data={this.isGetStartTime()}
                                    selectedValue={this.state.day_time_start}
                                    style={[styles.row, styles.select, components.input]}
                                    onValueChange={(day_time_start, itemIndex) => {
                                        this.setState({day_time_start})
                                    }}
                                />
                                <Text style={{paddingHorizontal: 10}}>-</Text>
                                <Select
                                    data={this.isGetEndTime()}
                                    selectedValue={this.state.day_time_end}
                                    style={[styles.row, styles.select, components.input]}
                                    onValueChange={(day_time_end, itemIndex) =>
                                        this.setState({day_time_end})
                                    }
                                />
                            </View>
                            {
                                (this.state.day_time_start >= this.state.day_time_end) && (
                                    <Text style={styles.error}>
                                        Не правильно
                                    </Text>
                                )
                            }
                            {
                                (!this.state.day_time_start || !this.state.day_time_end) && (
                                    <Text style={styles.error}>
                                        Заполните время
                                    </Text>
                                )
                            }
                        </View>
                        <View style={styles.bodyItem}>
                            <Text style={styles.bodyItemTitle}>Количество недель: </Text>
                            <View style={{width: '100%'}}>
                                <Select
                                    data={this.state.listWeeks}
                                    selectedValue={this.state.count_weeks}
                                    style={[styles.row, styles.select, components.input]}
                                    onValueChange={(count_weeks, itemIndex) => {
                                        console.log('count_weeks: ', count_weeks)
                                        this.setState({count_weeks})
                                    }}
                                />

                                {
                                    (!this.state.count_weeks) && (
                                        <Text style={styles.error}>
                                            Обязательно к заполнению
                                        </Text>
                                    )
                                }
                            </View>
                        </View>


                    </ScrollView>
                    <View style={styles.bottomContent}>
                        <Button full primary onPress={() => {
                            this.save()
                        }}>
                            <Text style={{color: 'white'}}>Сохранить</Text>
                        </Button>
                    </View>
                </KeyboardAvoidingView>
                {
                    (this.state.showDateStart) && (
                        <DateTimePicker
                            style={{width: '100%'}}
                            minimumDate={new Date()}
                            value={this.state.date_start}
                            onChange={(type, date_start) => {
                                this.setState({
                                    showDateStart: false,
                                })
                                if (date_start) {
                                    this.setState({
                                        date_start
                                    })
                                }
                            }}
                        />
                    )
                }
            </View>
        );
    }

    static
    navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Настройка расписания',
        };
    };
}

const
    styles = StyleSheet.create({
        page: {
            flex: 1
        },

        input: {
            flex: 1
        },
        select: {
            flex: 1,
            height: 100
        },
        bodyItem: {
            marginBottom: 10
        },

        bodyItemTitle: {
            fontSize: 16,
            marginBottom: 5
        },

        row: {
            flexDirection: 'row',
            alignItems: 'center',
        },
        error: {
            color: 'red',
            flex: 1
        },

        bottomContent: {
            paddingBottom: 10
        }
    })

export default TimeTableSettings
