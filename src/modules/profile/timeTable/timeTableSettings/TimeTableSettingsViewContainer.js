// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import TimeTableSettingsView from './TimeTableSettingsView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(TimeTableSettingsView);
