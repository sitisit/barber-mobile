import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    AsyncStorage,
    SafeAreaView
} from 'react-native';
import {Icon, Left, List, ListItem, Right, Badge} from "native-base";
import color from "../../../styles/color";
import {getPrice} from "../../../helper/formatting";
import axios from "../../../plugins/axios";
import {DropDownHolder} from "../../../components/DropDownAlert";
import {LoaderApp} from "../../../components/LoaderApp";


class PersonalArea extends Component {
    constructor(props) {
        super(props);

        this.state = {
            balance: 0
        }
    }

    componentDidMount = () => {
        this.getBalance()

        this.props.navigation.addListener('didFocus', () => {
            setTimeout(() => {
                this.getBalance()
            }, 500)
        });
    }

    getBalance = () => {
        axios('get', 'api/v1/get-available-balance').then(response => {
            this.setState({
                balance: response.data
            })
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        })
    }
    userExit = async () => {
        await AsyncStorage.removeItem('token')
        this.props.userExit()
    }

    render() {
        const { navigate } = this.props.navigation

        return (
            <View style={styles.page}>
                <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
                    <List>
                        <ListItem noIndent onPress={() => navigate('TimeTable')}>
                            <Left>
                                <Text>Расписание</Text>
                            </Left>
                            <Right>
                                <Icon
                                    name="chevron-right"
                                    type="MaterialIcons"
                                />
                            </Right>
                        </ListItem>
                        <ListItem noIndent onPress={() => navigate('Balance')}>
                            <Left>
                                <Text>
                                    <Text>Баланс&nbsp;</Text>
                                    <Text style={{fontWeight: 'bold'}}>{ getPrice(this.state.balance) }</Text>
                                </Text>
                            </Left>
                            <Right>
                                <Icon
                                    name="chevron-right"
                                    type="MaterialIcons"
                                />
                            </Right>
                        </ListItem>
                        <ListItem noIndent onPress={() => navigate('ContactDetails')}>
                            <Left>
                                <Text>Контактные данные</Text>
                            </Left>
                            <Right>
                                <Icon
                                    name="chevron-right"
                                    type="MaterialIcons"
                                />
                            </Right>
                        </ListItem>
                        <ListItem noIndent onPress={() => navigate('Accreditation')}>
                            <Left>
                                <Text>Аккредитации​</Text>
                            </Left>
                            <Right>
                                <Icon
                                    name="chevron-right"
                                    type="MaterialIcons"
                                />
                            </Right>
                        </ListItem>
                        {
                            (false) && <ListItem noIndent onPress={() => navigate('SettingFilterApplication')}>
                                <Left>
                                    <Text>Настройка фильтра заявок</Text>
                                </Left>
                                <Right>
                                    <Icon
                                        name="chevron-right"
                                        type="MaterialIcons"
                                    />
                                </Right>
                            </ListItem>
                        }
                        <ListItem noIndent onPress={() => navigate('TypeMaster')}>
                            <Left>
                                <Text>Тип мастера</Text>
                            </Left>
                            <Right>
                                <Icon
                                    name="chevron-right"
                                    type="MaterialIcons"
                                />
                            </Right>
                        </ListItem>
                        <ListItem noIndent onPress={() => navigate('Notification')}>
                            <Left style={{alignItems: 'center'}}>
                                <Text>Уведомления</Text>
                            </Left>
                            <Right>
                                <Icon
                                    name="chevron-right"
                                    type="MaterialIcons"
                                />
                            </Right>
                        </ListItem>
                    </List>
                </ScrollView>
                <View style={{paddingBottom: 10}}>
                    <ListItem noIndent onPress={() => this.userExit()} style={styles.listItemExit}>
                        <Left>
                            <Text style={{ color: 'white', fontSize: 15 }}>Выход</Text>
                        </Left>
                        <Right>
                            <Icon
                                name="sign-out-alt"
                                type="FontAwesome5"
                                style={{ color: 'white', fontSize: 15 }}
                            />
                        </Right>
                    </ListItem>
                </View>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Профиль',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: color.page,
    },

    badge: {
        backgroundColor: color.primary,
        marginLeft: 10,
        paddingHorizontal: 5,
        borderRadius: 100
    },

    listItemExit: {
        backgroundColor: color.primary,
        borderWidth: 0,
        borderStyle: 'solid',
    }
})

export default PersonalArea
