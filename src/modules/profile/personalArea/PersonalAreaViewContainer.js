// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';
import PersonalAreaView from './PersonalAreaView';

import { userExit } from './PersonalAreaState';

export default compose(
  connect(
    state => ({
        user: state.user
    }),
    dispatch => ({
        userExit: () => dispatch(userExit())
    }),
  ),
)(PersonalAreaView);
