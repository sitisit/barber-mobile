import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage
} from 'react-native';
import {NavigationActions, StackActions} from 'react-navigation';
import axios from "../../../plugins/axios";
import color from "../../../styles/color";


class Init extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    isToPage = (routeName) => {
        this.props.navigation.replace(routeName)
    }
    componentDidMount = () => {
        if (this.props.user.user) {
            this.isToPage('PersonalArea')
        } else {
            if (!this.isCheckToken()) {
                this.isToPage('Authorization')
            }
        }
    }
    isCheckToken = async () => {
        let token = await AsyncStorage.getItem('token')

        if (token) {
            axios('get', 'api/v1/me').then(response => {
                let user = response.data
                this.props.setUser(user)
                this.isToPage('PersonalArea')
            }).catch(error => {
                this.isToPage('Authorization')
            })
        } else {
            this.isToPage('Authorization')
        }

    }

    render() {
        return (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white'}}>
                <ActivityIndicator
                    animating
                    size="large"
                    color="#da7b9e"
                />
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: '',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        paddingVertical: 16,
        backgroundColor: color.page
    },
})

export default Init
