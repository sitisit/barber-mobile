import React from 'react';
import {
    Image,
    View,
    StyleSheet,
    Text,
    StatusBar
} from 'react-native';
import {createBottomTabNavigator, BottomTabBar} from 'react-navigation-tabs';
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';

//личный кабинет
import InitScreen from '../authorization/init/InitViewContainer'
import AuthorizationScreen from '../authorization/authorization/AuthorizationViewContainer'
import VirificationScreen from '../authorization/verification/VerificationViewContainer'


const iconApplicationFeed = require('../../../assets/icons/checklist.png');
const iconUser = require('../../../assets/icons/user.png')

const TabBarComponent = props => <BottomTabBar {...props} />;

const styles = StyleSheet.create({
    tabBarItemContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 5,
        color: '#939393',
    },
    tabBarIcon: {
        width: 20,
        height: 20,
        tintColor: '#000'
    },
    tabBarIconFocused: {
        tintColor: 'white',
    },
    tabBarTitle: {
        color: '#000',
        fontWeight: '300',
        marginTop: 2,
        fontSize: 12
    },
    tabBarTitleFocused: {
        color: 'white'
    },
});

export default createBottomTabNavigator(
    {
        PersonalArea: {
            screen: createStackNavigator({
                    Authorization: {
                        screen: AuthorizationScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            tabBarVisible: false
                        },
                    },
                    Virification: {
                        screen: VirificationScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#da7b9e',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white',
                            tabBarVisible: false
                        },

                    },
                },
                {
                    defaultNavigationOptions: {...TransitionPresets.SlideFromRightIOS}
                }
            ),
        },
    },
    {
        defaultNavigationOptions: ({navigation, screenProps}) => ({
            tabBarIcon: ({focused}) => {
                const {routeName} = navigation.state;
                let iconSource;

                switch (routeName) {
                    case 'PersonalArea':
                        iconSource = iconUser;
                        title = 'Профиль';
                        break;
                    default:
                        iconSource = iconHome;
                }
                return (
                    <View style={styles.tabBarItemContainer}>
                        <Image
                            resizeMode="contain"
                            source={iconSource}
                            style={[styles.tabBarIcon, focused && styles.tabBarIconFocused]}
                        />
                        <Text style={[styles.tabBarTitle, focused && styles.tabBarTitleFocused]}>{title}</Text>
                    </View>
                );
            },
            headerStyle: {
                backgroundColor: '#da7b9e'
            },
            tabBarComponent: (props) => {
                return null
            },
        }),
        initialRouteName: 'PersonalArea',
        tabBarPosition: 'bottom',
        animationEnabled: true,
        swipeEnabled: true,
        tabBarOptions: {
            showIcon: true,
            showLabel: false,
            style: {
                backgroundColor: '#da7b9e',
            },
            labelStyle: {
                color: '#7C7C7C',
                fontSize: 10,
                lineHeight: 12,
                padding: 0,
                margin: 0,
                fontWeight: '300'
            },
        },
    },
);
