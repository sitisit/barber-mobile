import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    AsyncStorage,
} from 'react-native';
import {Button} from 'native-base';
import typography from "../../../styles/typography";
import components from "../../../styles/components";
import color from "../../../styles/color";
import {TextInputMask} from "react-native-masked-text";
import {DropDownHolder} from "../../../components/DropDownAlert";
import {LoaderApp} from "../../../components/LoaderApp";
import axios from "../../../plugins/axios";
import * as Permissions from "expo-permissions";

const PUSH_ENDPOINT = 'pushers/recipient/create';

class Authorization extends Component {
    constructor(props) {
        super(props);

        this.state = {
            phone: '',
            code: '',
        }
    }

    componentDidMount = () => {
        this.setState({
            phone: this.props.navigation.state.params.phone
        })
    }

    setToken = async (token) => {
        try {
            await AsyncStorage.setItem('token', token);
            this.userMe()
        } catch (error) {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
        }
    }

    userMe = () => {
        axios('get', 'api/v1/me').then(response => {
            this.props.setUser(response.data)
            LoaderApp.isChangeLoaderApp(false)
            this.props.navigation.replace('Init')
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message);
            LoaderApp.isChangeLoaderApp(false)
        })
    }
    isLogin = () => {
        if (this.state.code) {
            LoaderApp.isChangeLoaderApp(true)

            let body = {
                tel: this.state.phone,
                code: this.state.code
            }

            axios('post', 'api/v1/login', body).then(response => {
                this.setToken(response.data)
            }).catch(error => {
                LoaderApp.isChangeLoaderApp(false)
                DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
            })
        } else {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Не заполнен "Код из СМС"')
        }
    }

    render() {
        return (
            <View style={styles.page}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{padding: 20, flex: 1}}
                >
                    <View style={{marginBottom: 20}}>
                        <Text style={[typography.heading, styles.heading]}>Подтверждение</Text>
                        <Text style={{marginBottom: 10}}>На номер телефона {this.state.phone} было отправлено СМС</Text>
                        <View style={{marginBottom: 10}}>
                            <TextInputMask
                                type={'custom'}
                                options={{
                                    mask: '999999',
                                }}
                                value={this.state.code}
                                style={[components.input, styles.input]}
                                placeholder="Код из СМС"
                                onChangeText={(code) => {
                                    this.setState({code})
                                }}
                                placeholderTextColor={color.placeholder}
                                keyboardType="number-pad"

                                returnKeyLabel='Далее'
                                returnKeyType='send'
                                onSubmitEditing={() => this.isLogin()}
                            />
                        </View>
                    </View>
                    <View style={{paddingBottom: 10}}>
                        <Button block onPress={() => this.isLogin()}>
                            <Text style={{color: 'white'}}>Подтвердить</Text>
                        </Button>
                    </View>
                </ScrollView>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: '',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: color.page,
    },
    heading: {
        textAlign: 'center',
        marginBottom: 20
    }
})

export default Authorization
