// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import VerificationView from './VerificationView';

import { setUser } from '../../profile/personalArea/PersonalAreaState'

export default compose(
  connect(
    state => ({
        user: state.user
    }),
    dispatch => ({
        setUser: (user) => dispatch(setUser(user)),
    }),
  ),
)(VerificationView);
