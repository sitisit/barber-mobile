import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    KeyboardAvoidingView,
    Dimensions
} from 'react-native';
import {Button} from 'native-base';
import typography from "../../../styles/typography";
import components from "../../../styles/components";
import color from "../../../styles/color";
import {TextInputMask} from "react-native-masked-text";
import {DropDownHolder} from "../../../components/DropDownAlert";
import {LoaderApp} from "../../../components/LoaderApp";
import axios from "../../../plugins/axios";

const {height} = Dimensions.get('window');

class Authorization extends Component {
    constructor(props) {
        super(props);

        this.state = {
            phone: ''
        }
    }

    componentDidMount = () => {
    }

    isSendCode = () => {
        let validator = /^\+7(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{2}(-|\s)\d{2}$/;

        if (!this.state.phone.match(validator)){
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Неправильно набран номер')
            return false
        }

        if (this.state.phone) {
            LoaderApp.isChangeLoaderApp(true)

            let body = {
                phone: this.state.phone
            }

            axios('post', 'api/v1/getcode', body).then(response => {
                LoaderApp.isChangeLoaderApp(false)
                this.props.navigation.replace('Virification', {
                    phone: this.state.phone
                })
            }).catch(error => {
                if (error.response.status == 400) {
                    DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Вы не зарегистрированы')
                } else {
                    DropDownHolder.dropDown.alertWithType('error', 'Ошибка', error.response.data.message)
                }

                LoaderApp.isChangeLoaderApp(false)
            })
        } else {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Не заполнен "Номер телефона"')
        }
    }

    render() {
        return (
            <View style={styles.page}>
                <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{padding: 20}}>
                    <View style={{marginBottom: 20}}>
                        <Text style={[typography.heading, styles.heading]}>Авторизация</Text>
                        <View style={{marginBottom: 10}}>
                            <TextInputMask
                                type={'custom'}
                                options={{
                                    mask: '+7(999)999-99-99',
                                }}
                                value={this.state.phone}
                                style={[components.input, styles.input]}
                                placeholder="Номер телефона"
                                onChangeText={(phone) => {
                                    this.setState({phone})
                                }}
                                placeholderTextColor={color.placeholder}
                                keyboardType="number-pad"

                                returnKeyLabel='Далее'
                                returnKeyType='send'
                                onSubmitEditing={() => this.isSendCode()}
                            />
                        </View>
                    </View>
                    <View style={{paddingBottom: 10}}>
                        <Button block onPress={() => this.isSendCode()}>
                            <Text style={{color: 'white'}}>Войти</Text>
                        </Button>
                    </View>
                </ScrollView>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: '',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: color.page,
    },
    heading: {
        textAlign: 'center',
        marginBottom: 20
    }
})

export default Authorization
