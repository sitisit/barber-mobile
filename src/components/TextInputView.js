import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    TextInput,
    Text
} from 'react-native';
import components from "../styles/components";

// style={} - кастомный стили контейнера
// styleLabel={} - кастомный стиль лейбла
// styleInput={} - костомный стиль инпута

class TextInputView extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isViewInput: false
        }
    }


    render() {
        return (
            <View style={[styles.component, this.props.style]}>
                <Text style={styles.componentTitle}>{this.props.title}</Text>
                <View style={styles.componentContent}>
                    {
                        (this.props.children)? (
                            this.props.children
                        ): (
                            <TextInput
                                value={this.props.value}
                                placeholder={this.props.placeholder}
                                onChangeText={(value) => {
                                    this.props.onChangeText(value)
                                }}
                                style={components.input}
                                keyboardType={this.props.keyboardType}
                            />
                        )
                    }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    component: {},
    componentTitle: {
        fontSize: 16,
    color: '#565656',
        marginBottom: 3
    },
    componentContent: {},
})

export default TextInputView
