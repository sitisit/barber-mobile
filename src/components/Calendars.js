import React, {Component} from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import {Agenda, LocaleConfig} from 'react-native-calendars';
import ApplicationCard from './ApplicationCard'
import {LoaderApp} from "./LoaderApp";
import NotDataBlock from "./NotDataBlock";

LocaleConfig.locales['ru'] = {
    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    monthNamesShort: ['Янв.', 'Фев.', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Авг.', 'Сент.', 'Окт.', 'Ноя.', 'Дек.'],
    dayNames: ['Восресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    dayNamesShort: ['Вс.', 'Пн.', 'Вт.', 'Ср.', 'Чт.', 'Пт.', 'Сб.'],
    today: 'Сегодня'
};
LocaleConfig.defaultLocale = 'ru';

export default class Calendars extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: {},
        };

        this.refAgenda = React.createRef();
    }

    componentDidMount = () => {
        let date = new Date()

        let day = {
            year: date.getFullYear(),
            month: date.getMonth(),
            day: date.getDay(),
            timestamp: Date.now(),
            dateString: ''
        }

        this.loadItems(day)
    }
    initCalendar = (day) => {
        for (let i = 0; i < 70; i++) {
            const time = day.timestamp + i * 24 * 60 * 60 * 1000;
            const strTime = this.timeToString(time);
            this.state.items[strTime] = [];
        }
        const newItems = {};
        Object.keys(this.state.items).forEach(key => {
            newItems[key] = this.state.items[key];
        });

        return newItems
    }
    loadItems = (day) => {
        LoaderApp.isChangeLoaderApp(true)
        let data = this.props.data
        let items = this.initCalendar(day)

        if (data.length > 0) {
            data.map(request => {
                let date = request.want_date.split('.').reverse().join('-')

                if (!items[date]) {
                    items[date] = []
                }

                items[date].push(request)
            })

            this.setState({
                items
            })
        }

        LoaderApp.isChangeLoaderApp(false)
    }
    renderItem = (item) => {
        return (
            <TouchableOpacity onPress={() => this.props.toApplication(item)}>
                <ApplicationCard
                    data={item}
                />
            </TouchableOpacity>
        );
    }
    renderEmptyDate = () => {
        return (
            <View style={styles.emptyDate}>
                <Text>На текущей день нет активных заявок</Text>
            </View>
        )
    }
    timeToString = (time) => {
        const date = new Date(time);
        return date.toISOString().split('T')[0];
    }

    renderDay = (day, item) => {
        if (item && day) {
            return (
                <View>
                    <Text>{day.day}</Text>
                </View>
            )
        } else {
            return <View style={{display: 'none', position: 'absolute', zIndex: -9999}}/>
        }
    }

    render() {
        if (Object.keys(this.state.items).length == 0) {
            return (
                <NotDataBlock
                    title={'Заявок нет'}
                />
            )
        }

        return (
            <Agenda
                ref={this.refAgenda}
                items={this.state.items}
                loadItemsForMonth={this.loadItems.bind(this)}
                renderItem={(item) => this.renderItem(item)}
                renderEmptyDate={() => this.renderEmptyDate()}
                rowHasChanged={(r1, r2) => {
                    return r1.text !== r2.text
                }}
                theme={{'stylesheet.agenda.list': {container: styles.agentaContainer}}}
                firstDay={1}
                pastScrollRange={2}
                futureScrollRange={2}
                selected={new Date()}
            />
        );
    }
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: 'red',
        flex: 1,
        borderRadius: 5,
        paddingLeft: 10,
        marginRight: 10,
        justifyContent: 'center',
        marginTop: 5,

    },
    emptyDate: {
        backgroundColor: 'white',
        padding: 15,
        borderRadius: 5,
    },

    agentaContainer: {
        paddingTop: 10,
        paddingRight: 20,
        flexDirection: 'row',
    }
});
