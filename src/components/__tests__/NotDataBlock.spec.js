/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';

import {
  NotDataBlock,
} from '../index';

describe('NotDataBlock Component', () => {
  it('renders as expected', () => {
  const wrapper = shallow(
    <NotDataBlock />,
  );
  expect(wrapper).toMatchSnapshot();
});
