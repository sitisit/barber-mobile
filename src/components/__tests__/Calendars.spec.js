/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';

import {
  Calendars,
} from '../index';

describe('Calendars Component', () => {
  it('renders as expected', () => {
  const wrapper = shallow(
    <Calendars />,
  );
  expect(wrapper).toMatchSnapshot();
});
