/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';

import {
  TextInputView,
} from '../index';

describe('TextInputView Component', () => {
  it('renders as expected', () => {
  const wrapper = shallow(
    <TextInputView />,
  );
  expect(wrapper).toMatchSnapshot();
});
