/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';

import {
  ModalHideBottom,
} from '../index';

describe('ModalHideBottom Component', () => {
  it('renders as expected', () => {
  const wrapper = shallow(
    <ModalHideBottom />,
  );
  expect(wrapper).toMatchSnapshot();
});
