/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';

import {
  Filter,
} from '../index';

describe('Filter Component', () => {
  it('renders as expected', () => {
  const wrapper = shallow(
    <Filter />,
  );
  expect(wrapper).toMatchSnapshot();
});
