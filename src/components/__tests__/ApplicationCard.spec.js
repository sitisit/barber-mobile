/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';

import {
  ApplicationCard,
} from '../index';

describe('ApplicationCard Component', () => {
  it('renders as expected', () => {
  const wrapper = shallow(
    <ApplicationCard />,
  );
  expect(wrapper).toMatchSnapshot();
});
