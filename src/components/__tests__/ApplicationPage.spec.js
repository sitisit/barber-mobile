/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';

import {
  ApplicationPage,
} from '../index';

describe('ApplicationPage Component', () => {
  it('renders as expected', () => {
  const wrapper = shallow(
    <ApplicationPage />,
  );
  expect(wrapper).toMatchSnapshot();
});
