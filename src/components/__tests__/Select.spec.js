/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';

import {
  Select,
} from '../index';

describe('Select Component', () => {
  it('renders as expected', () => {
  const wrapper = shallow(
    <Select />,
  );
  expect(wrapper).toMatchSnapshot();
});
