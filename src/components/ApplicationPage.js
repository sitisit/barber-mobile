import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    ScrollView,
    Text
} from 'react-native';
import {Button, Icon} from "native-base";
import {getPrice, getTimeHours} from "../helper/formatting";
import ModalHideBottom from "./ModalHideBottom";
import {LoaderApp} from "./LoaderApp";
import color from "../styles/color";
import applyLayout from "react-native-web/dist/modules/applyLayout";

class ApplicationPage extends Component {
    constructor(props) {
        super(props)

        this.state = {}
    }

    takeWork = () => {
        LoaderApp.isChangeLoaderApp(true)
        this.props.takeWork(this.props.data)
    }

    render() {
        const application = this.props.data

        console.log('application: ', (application.paints != ''))

        return (
            <View style={styles.page}>
                <View style={styles.top}>
                    <Text style={styles.name}>{application.service_name}</Text>
                    <Text style={styles.category}>{application.category_name}</Text>
                </View>
                <View style={styles.lineInfo}>
                    <View style={styles.lineInfoItem}>
                        <View style={[styles.lineInfoItemIcon, {backgroundColor: '#308c65'}]}>
                            <Icon
                                name="ruble-sign"
                                type="FontAwesome5"
                                style={{color: 'white', fontSize: 10}}
                            />
                        </View>
                        <Text style={styles.lineInfoItemText}>
                            {getPrice(application.master_price)} - вознаграждение мастера
                        </Text>
                    </View>
                    {
                        (false) && (
                            <View style={styles.lineInfoItem}>
                                <View style={[styles.lineInfoItemIcon, {backgroundColor: '#308c65'}]}>
                                    <Icon
                                        name="ruble-sign"
                                        type="FontAwesome5"
                                        style={{color: 'white', fontSize: 10}}
                                    />
                                </View>
                                <Text style={styles.lineInfoItemText}>
                                    {getPrice(application.total_price)} - стоимость для клиента
                                </Text>
                            </View>
                        )
                    }
                    <View style={styles.lineInfoItem}>
                        <View style={[styles.lineInfoItemIcon, {backgroundColor: '#0a6c82'}]}>
                            <Icon
                                name="clock"
                                type="FontAwesome5"
                                style={{color: 'white', fontSize: 10}}
                            />
                        </View>
                        <Text style={styles.lineInfoItemText}>
                            ~ {getTimeHours(application.lead_time)} - время выполнения заявки
                        </Text>
                    </View>
                </View>
                <View style={styles.sectionItem}>
                    <Text style={styles.text}>
                        <Text style={styles.label}>Дата и время ожидание: </Text>
                        {application.want_date} {application.want_time}
                    </Text>
                </View>

                <View style={[styles.sectionItem, {flexDirection: 'column'}]}>
                    <Text style={[styles.text, {marginBottom: 10}]}>
                        <Text style={styles.label}>Район: </Text>
                        {application.district}
                    </Text>
                </View>

                {
                    (application.subcategories != '') && (
                        <View style={styles.sectionItem}>
                            <Text style={styles.text}>
                                <Text style={styles.label}>Длина волос: </Text>
                                { application.subcategories }
                            </Text>
                        </View>
                    )
                }

                {
                    (application.options != '') && (
                        <View style={styles.sectionItem}>
                            <Text style={styles.text}>
                                <Text style={styles.label}>Дополнительные работы: </Text>
                                { application.options }
                            </Text>
                        </View>
                    )
                }

                {
                    ( application.paints != '') && (
                        <View style={styles.sectionItem}>
                            <Text style={styles.text}>
                                <Text style={styles.label}>Краска: </Text>
                                <Text>{ application.paints }</Text>
                            </Text>
                        </View>
                    )
                }

                {
                    (application.user_comment.length > 0) && (
                        <View style={styles.blockComment}>
                            <Text style={styles.blockCommentLabel}>Комментарий пользователя</Text>
                            <View style={styles.blockCommentContent}>
                                <Text style={styles.blockCommentText}>{application.user_comment}</Text>
                            </View>
                        </View>
                    )
                }
                {
                    (application.operator_comment.length > 0) && (
                        <View style={styles.blockComment}>
                            <Text style={styles.blockCommentLabel}>Комментарий пользователя</Text>
                            <View style={styles.blockCommentContent}>
                                <Text style={styles.blockCommentText}>{application.operator_comment}</Text>
                            </View>
                        </View>
                    )
                }

                <View style={styles.bottomContent}>
                    <Button full primary onPress={() => this.takeWork()}>
                        <Text style={{color: 'white'}}>Выполнить</Text>
                    </Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        padding: 20
    },
    top: {
        marginBottom: 10
    },
    name: {
        fontSize: 18,
        lineHeight: 20,
        fontWeight: 'bold'
    },
    category: {
        fontSize: 20,
        lineHeight: 22
    },
    image: {
        flex: 1
    },

    lineInfo: {
        flexWrap: 'wrap',
        marginBottom: 10,
    },
    lineInfoItem: {
        marginBottom: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    lineInfoItemIcon: {
        width: 20,
        height: 20,
        padding: 5,
        backgroundColor: 'red',
        borderRadius: 50,
        marginRight: 5,

        justifyContent: 'center',
        alignItems: 'center'
    },
    lineInfoItemText: {
        fontSize: 16,
        fontWeight: 'bold',
        flex: 1
    },

    sectionItem: {
        flexDirection: 'row',
        marginBottom: 10
    },
    label: {
        fontWeight: 'bold'
    },
    text: {
        fontSize: 18,
        lineHeight: 20
    },

    bottomContent: {
        paddingBottom: 10
    },

    blockComment: {
        marginBottom: 10,
    },
    blockCommentLabel: {
        fontWeight: 'bold',
        marginBottom: 5
    },
    blockCommentContent: {
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 2
    }
})

export default ApplicationPage
