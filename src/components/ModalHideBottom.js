import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    Dimensions, TouchableWithoutFeedback, ScrollView
} from 'react-native';
import RBSheet from "react-native-raw-bottom-sheet";

const heightScreen = Dimensions.get('window').height;

class ModalHideBottom extends Component {
    constructor(props) {
        super(props)

        this.state = {}

        this.ModalHideBottom = React.createRef()
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if (prevProps.open != this.props.open && this.props.open) {
            this.ModalHideBottom.open()
        }
        if (prevProps.open != this.props.open && !this.props.open) {
            this.ModalHideBottom.close()
        }
    }

    render() {


        return (
            <RBSheet
                ref={ref => {
                    this.ModalHideBottom = ref
                }}
                height={heightScreen - 50}
                onClose={() => this.props.close()}
                closeOnDragDown
            >
                <ScrollView
                    showsVerticalScrollIndicator={false}
                >
                    <TouchableWithoutFeedback>
                        <View style={styles.gridContainer}>
                            { this.props.children }
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </RBSheet>
        )
    }
}

const styles = StyleSheet.create({
    gridContainer: {
        flex: 1
    }
})

export default ModalHideBottom
