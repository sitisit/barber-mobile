import React, {Component} from 'react'
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

class NotDataBlock extends Component {
    constructor(props) {
        super(props)

        this.state = {}
    }

    render() {
        return (
            <View style={styles.block}>
                <Text style={styles.blockTitle}>{this.props.title}</Text>
                <Text style={styles.blockText}>{this.props.text}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    block: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    blockTitle: {
        fontSize: 22,
        fontWeight: 'bold',
    },
    blockText: {
        fontSize: 18
    },
})

export default NotDataBlock
