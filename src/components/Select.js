// https://github.com/lawnstarter/react-native-picker-select

import React, {Component} from 'react'
import {
    View,
    StyleSheet
} from 'react-native';
import PropTypes from 'prop-types';
import RNPickerSelect, {defaultStyles} from 'react-native-picker-select';
import components from "../styles/components";

export const Select = (props) => {
    let styles = pickerSelectStyles
    let useNativeAndroidPickerStyle = true

    if ( props.style ){
        props.style.map(item => {
            styles['inputIOS'] = {...styles.inputIOS, ...item}
            styles['inputAndroid'] = {...styles.inputAndroid, ...item}
        })
    }
    if ( props.useNativeAndroidPickerStyle ){
        useNativeAndroidPickerStyle = false
    }

    return (
        <View style={{flex: 1}}>
            <RNPickerSelect
                onValueChange={(item) => {
                    props.onValueChange(item)
                }}
                value={props.selectedValue}
                items={props.data}
                style={styles}
                placeholder={{label: (props.placeholder)? props.placeholder: 'Выберите'}}
                useNativeAndroidPickerStyle={useNativeAndroidPickerStyle}
            />
        </View>
    )
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 12,
        color: 'black',
        paddingRight: 30,
        flex: 1,
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 12,
        color: 'black',
        paddingRight: 30,
        flex: 1,
    },
});
