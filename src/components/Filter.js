import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    Dimensions,
    Text,
    ScrollView,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Animated
} from 'react-native';
import {Separator, Accordion, Body, Button, CheckBox, Icon, Left, List, ListItem, Right} from "native-base";
import RBSheet from "react-native-raw-bottom-sheet";
import {LoaderApp} from "./LoaderApp";
import {getPrice} from "../helper/formatting";

const heightScreen = Dimensions.get('window').height;
const AnimatedView = Animated.createAnimatedComponent(View);

class Filter extends Component {
    constructor(props) {
        super(props)

        this.state = {
            filter: [],
            filterOld: [],

            listExpanded: [],

            changeFilter: false
        }

        this.Scrollable = React.createRef()
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if (prevProps.open != this.props.open && this.props.open) {
            this.Scrollable.open()
            if (this.state.filter.length == 0) {
                this.loadFilter()
            }
        }
        if (prevProps.open != this.props.open && !this.props.open) {
            this.Scrollable.close()
        }
    }

    loadFilter = () => {
        LoaderApp.isChangeLoaderApp(true)
        let filter = [
            {
                id: "1",
                name: "Категория",
                variants: [
                    {label: "Женские", value: 73},
                    {label: "Мужские", value: 395},
                    {label: "Детские", value: 574}
                ],
                availables: ["73", "395", "574"],
                default: [],
                filterName: "v[5][equal][]",
            },
        ]

        setTimeout(() => {
            this.setState({
                filter: filter,
                filterOld: JSON.parse(JSON.stringify(filter))
            })

            LoaderApp.isChangeLoaderApp(false)
        }, 0)
    }

    isCheckFilterItem = (variant, filter) => {
        let idx = filter.default.indexOf(variant.value)

        if (idx > -1) {
            filter.default.splice(idx, 1)
        } else {
            filter.default.push(variant.value)
        }

        this.setState({filter: this.state.filter})

        this.isChangeFilter()
    }
    isOpenFilter = (item) => {
        let expanded = this.state.listExpanded

        if (expanded.indexOf(item.id) == -1) {
            expanded.push(item.id)
        } else {
            expanded.splice(expanded.indexOf(item.id), 1)
        }

        this.setState({
            listExpanded: expanded
        })
    }
    isChangeFilter = () => {
        let filter = JSON.stringify(this.state.filter)
        let filterOld = JSON.stringify(this.state.filterOld)

        if (filter == filterOld) {
            this.setState({changeFilter: false})
        } else {
            this.setState({changeFilter: true})
        }
    }

    _renderHeader = (item) => {
        let expanded = false

        if (this.state.listExpanded.indexOf(item.id) > -1) {
            expanded = true
        }

        return (
            <View
                style={[styles.filterHeader, (expanded) ? styles.filterHeaderActive : '']}
            >
                <Text style={{fontWeight: "600"}}>
                    {item.name} {(item.default.length > 0) ? '(' + item.default.length + ')' : ''}
                </Text>
                {expanded
                    ? <Icon
                        style={{fontSize: 15}}
                        name="angle-up"
                        type={'FontAwesome5'}
                    />
                    : <Icon
                        style={{fontSize: 15}}
                        name="angle-down"
                        type={'FontAwesome5'}
                    />
                }
            </View>
        );
    }
    _renderContent = (item) => {
        return (
            <ScrollView
                style={{flex: 1}}
                showsVerticalScrollIndicator={false}
            >
                {
                    item.variants.map((variant, idx) => (
                        <ListItem
                            key={'filter- ' + variant.value}
                            style={{borderBottomWidth: 0}}
                            noIndent
                            onPress={() => this.isCheckFilterItem(variant, item)}
                        >
                            <CheckBox
                                checked={item.default.indexOf(variant.value) > -1}
                                onPress={() => this.isCheckFilterItem(variant, item)}/>
                            <Body style={{marginLeft: 5}}>
                                <Text>{variant.label}</Text>
                            </Body>
                        </ListItem>
                    ))
                }
            </ScrollView>
        );
    }

    render() {
        return (
            <RBSheet
                ref={ref => {
                    this.Scrollable = ref
                }}
                height={heightScreen - 50}
                onClose={() => this.props.close()}
                closeOnDragDown
            >
                <ScrollView
                    style={{flex: 1}}
                    showsVerticalScrollIndicator={false}
                >
                    <TouchableWithoutFeedback>
                        <View style={styles.gridContainer}>
                            <ScrollView
                                style={{flex: 1}}
                                showsVerticalScrollIndicator={false}
                            >
                                <Separator bordered style={styles.separator}>
                                    <Text style={{marginRight: 10}}>
                                        Сортировка
                                    </Text>
                                    <Icon
                                        style={{fontSize: 15}}
                                        name="sort"
                                        type="MaterialIcons"
                                    />
                                </Separator>
                                <ListItem noIndent onPress={() => {this.props.isChangeSort(2)}}>
                                    <Left>
                                        <Text>По новизне</Text>
                                    </Left>
                                </ListItem>
                                <ListItem noIndent onPress={() => {this.props.isChangeSort(1)}}>
                                    <Left>
                                        <Text>По дате выполнения</Text>
                                    </Left>
                                </ListItem>
                            </ScrollView>
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </RBSheet>
        )
    }
}

const styles = StyleSheet.create({
    gridContainer: {
        flex: 1
    },

    container: {
        padding: 20,
        flex: 1
    },

    separator: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    filterCard: {
        borderTopWidth: 1,
        borderStyle: 'solid',
        borderColor: 'black'
    },
    filterHeader: {
        flexDirection: "row",
        padding: 10,
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: "white",
    },
    filterHeaderActive: {
        backgroundColor: 'rgba(218, 123, 158, 0.1)'
    },
    filterBody: {},

    bottomContent: {
        position: 'relative',
        paddingBottom: 10,
        bottom: -100
    }
})

export default Filter
