import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    Text,
    TouchableWithoutFeedback,
    TouchableOpacity
} from 'react-native';
import {Icon} from "native-base";
import {getTimeHours, getPrice} from "../helper/formatting";
import color from "../styles/color";

class ApplicationCard extends Component {
    constructor(props) {
        super(props)

        this.state = {}
    }


    render() {
        const application = this.props.data

        return (
            <View style={[styles.card, this.props.style]}>
                <Text>{application.service_name} ({application.category_name})</Text>
                <Text>Номер заявки: {application.id}</Text>
                <View style={styles.cardLine}>
                    <Text>
                        <Text>Стоимость для клиента: </Text>
                        {getPrice(application.total_price)}
                    </Text>
                </View>
                <View style={styles.cardLine}>
                    <Text>
                        <Text>Штраф мастера: </Text>
                        {getPrice(application.master_fine)}
                    </Text>
                </View>
                {
                    (application.reason)&&(
                        <View style={styles.cardLine}>
                            <Text>
                                <Text>Причина отмены заявки: </Text>
                                {application.reason}
                            </Text>
                        </View>
                    )
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    card: {
        backgroundColor: 'white',
        padding: 15,
        borderRadius: 5,
        marginBottom: 20
    },
    cardLine: {
        flexDirection: 'row',
        maxWidth: '100%'
    },

    buttonHide: {
        width: 30,
        height: 30,
        backgroundColor: color.primary,
        borderRadius: 2,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default ApplicationCard
