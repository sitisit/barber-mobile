import React, {Component} from 'react'
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    FlatList,
    TouchableWithoutFeedback
} from 'react-native';
import components from "../styles/components";

class InputDropdown extends Component {
    constructor(props) {
        super(props)

        this.state = {
            showList: false
        }

        this.refInput = React.createRef();
    }

    clickItem = (item) => {
        this.props.onItemSelect(item)
        this.setState({
            showList: false
        })
    }

    render() {
        return (
            <View
                style={styles.block}
            >
                <TextInput
                    ref={this.refInput}
                    value={this.props.value}
                    onChangeText={text => {
                        this.props.onChangeText(text)
                    }}
                    style={[styles.row, components.input, this.props.styleInput]}
                    placeholder={'Введите'}
                    onFocus={() => this.setState({showList: true})}
                    onBlur={() => {setTimeout(() => {this.setState({showList: false})}, 10000)}}
                />
                {
                    (this.props.list.length > 0 && this.state.showList) && (
                        <FlatList
                            data={this.props.list}
                            style={[styles.list, this.props.styleFlatList]}
                            keyExtractor={item => item.id}
                            renderItem={({item}) => (
                                <TouchableWithoutFeedback
                                    onPress={() => this.clickItem(item)}
                                >
                                    <View style={styles.listItem}>
                                        <Text>{item.name}</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            )}
                        />
                    )
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    block: {
        position: 'relative',
        zIndex: 5
    },
    list: {
        marginTop: 3,
        backgroundColor: 'white',
        borderRadius: 3,
        maxHeight: 140,
        zIndex: 10,
        width: '100%',
    },
    listItem: {
        paddingHorizontal: 5,
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: 'rgba(0, 0, 0, 0.25)'
    },
})

export default InputDropdown
