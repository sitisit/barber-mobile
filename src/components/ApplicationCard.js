import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    Text,
    TouchableWithoutFeedback,
    TouchableOpacity
} from 'react-native';
import {Icon} from "native-base";
import {getTimeHours, getPrice} from "../helper/formatting";
import color from "../styles/color";

class ApplicationCard extends Component {
    constructor(props) {
        super(props)

        this.state = {}
    }

    getStatusApplication = (urgency) => {
        if ( urgency == 1 ){
            return (<Text style={{fontWeight: 'bold', fontSize: 16}}>Сверх срочная</Text>)
        }
        if ( urgency == 2 ){
            return (<Text style={{fontWeight: 'bold', fontSize: 16}}>Срочная</Text>)
        }

        return null
    }
    getStyleApplication = (urgency) => {
        if ( urgency == 1 ){
            return {
                borderColor: '#ad0000'
            }
        }
        if ( urgency == 2 ){
            return {
                borderColor: color.primary
            }
        }
    }

    render() {
        const application = this.props.data

        console.log('application: ', application)

        return (
            <View style={[styles.card, this.props.style, this.getStyleApplication(application.urgency)]}>
                <View style={styles.cardTop}>
                    <View style={[styles.cardStatus, this.getStyleApplication(application.urgency)]}>
                        <Text style={{
                            marginRight: 5,
                            fontWeight: 'bold',
                            fontSize: 16
                        }}>№ {application.id}</Text>
                        {
                            this.getStatusApplication(application.urgency)
                        }
                    </View>
                    {
                        (this.props.hideRequest) && (
                            <View style={styles.cardHide}>
                                <TouchableOpacity style={styles.buttonHide} onPress={() => this.props.hideRequest(application)}>
                                    <Icon
                                        name="eye-slash"
                                        type="FontAwesome5"
                                        style={{ color: 'white', fontSize: 20 }}
                                    />
                                </TouchableOpacity>
                            </View>
                        )
                    }
                </View>

                <View style={[styles.cardLine, { marginBottom: 5 }]}>
                    <Text style={{ fontSize: 14, fontWeight: 'bold' }}>
                        {application.want_date} {application.want_time}
                    </Text>
                </View>


                <View style={[styles.cardLine, { marginBottom: 5 }]}>
                    <Text style={{ fontSize: 15, fontWeight: 'bold' }}>
                        {application.service_name} ({application.category_name})
                    </Text>
                </View>

                <View style={styles.cardLine}>
                    <Text>
                        <Text>Район: </Text>{application.district}
                    </Text>
                </View>
                <View style={styles.cardLine}>
                    <Text>
                        <Text>Расчетное время выполнения заявки: </Text>
                        {getTimeHours(application.lead_time)}
                    </Text>
                </View>
                {
                    (false) && (
                        <View style={styles.cardLine}>
                            <Text>
                                <Text>Стоимость для клиента: </Text>
                                {getPrice(application.total_price)}
                            </Text>
                        </View>
                    )
                }

                <View style={[styles.cardLine, { marginTop: 5 }]}>
                    <Text style={{ fontSize: 15, fontWeight: 'bold' }}>
                        <Text>Вознаграждение мастера: </Text>
                        {getPrice(application.master_price)}
                    </Text>
                </View>
                <Text style={{ fontSize: 10, opacity: 0.6 }}>Стоимость расходных материалов входит в сумму вознаграждения</Text>


                {
                    ( application.deposit ) && (
                        <View>
                            <View style={[styles.cardLine, { marginTop: 5 }]}>
                                <Text style={{ fontSize: 15, fontWeight: 'bold' }}>
                                    <Text>Необходимый депозит: </Text>
                                    {getPrice(application.deposit)}
                                </Text>
                            </View>
                            <Text style={{ fontSize: 10, opacity: 0.6 }}>{ application.deposit_status }</Text>
                        </View>
                    )
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    card: {
        backgroundColor: 'white',
        padding: 15,
        borderRadius: 5,
        marginBottom: 20,

        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#00b5ee',

        overflow: 'hidden'
    },
    cardLine: {
        flexDirection: 'row',
        maxWidth: '100%'
    },

    buttonHide: {
        width: 45,
        height: 45,
        backgroundColor: color.primary,
        justifyContent: 'center',
        alignItems: 'center',
    },

    cardTop: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    cardStatus: {
        flexDirection: 'row',
        alignItems: 'center',

        height: 45,

        marginLeft: -15,
        marginTop: -15,
        paddingHorizontal: 15,
        paddingVertical: 5,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: '#00b5ee',
    },
    cardHide: {
        justifyContent: 'center',
        alignItems: 'center',

        marginRight: -15,
        marginTop: -15,

        height: 45,
        width: 45,
    },
})

export default ApplicationCard
